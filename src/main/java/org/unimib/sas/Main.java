package org.unimib.sas;

import org.unimib.sas.commands.AbstractCommand;
import org.unimib.sas.commands.ICommand;

public class Main {
    //TODO: inform user what option is missing (AbstractCommand)
    public static void main(String[] args) {
        if(args == null || args.length == 0)
            args = new String[]{"help"}; // print help if no arg

        System.out.println("SSCM -- Source Sink Classification Model\n");
        ICommand command = AbstractCommand.parseArgs(args);
        command.run();

    }

    private void voiD(String[] args) {
        args = ("train -t scratchdir/final-dataset-Category.csv " +
                "-o scratchdir/model.weka -m weka.classifiers.functions.SMO").split(" ");

        args = ("classify -cp scratchdir/classpath/ " +
                "-i scratchdir/tinylog-1.3.2.jar -o scratchdir/tinylog-classify.csv -m scratchdir/model.weka " +
                "-r scratchdir/categorized-resources.yml ").split(" ");

        args = ("generate -cp scratchdir/classpath -l ./scratchdir/final-dataset.csv -o ./scratchdir/final-dataset-Category.csv " +
                "-r ./scratchdir/categorized-resources.yml").split(" ");

        args = ("list -f scratchdir/classpath/httpclient/httpclient.jar").split(" ");
    }
}
