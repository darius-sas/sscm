package org.unimib.sas.data.cast;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import soot.SootMethod;

import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Pattern;

/**
 * Specific parser for input and target methods from cast web page (used for validation)
 */
public class ValidationMethodsParser extends CastParser {

    private static final Logger logger = LoggerFactory.getLogger(ValidationMethodsParser.class.getName());

    private File file;

    private Set<SootMethod> inputMethods;
    private Set<SootMethod> targetMethods;

    private Pattern INPUT_METHODS_CURSOR = Pattern.compile("Input methods");
    private Pattern TARGET_METHODS_CURSOR = Pattern.compile("Target methods");
    private Pattern METHOD_LINE = Pattern.compile("\\[.*\\)");

    public ValidationMethodsParser(String path){
        this.file = new File(path);
        assert file.exists();
        assert !file.isDirectory();

        this.targetMethods = new HashSet<>();
        this.inputMethods = new HashSet<>();

    }

    public void parse(){
        reset();
        Set<SootMethod> temp = null;
        try(Scanner scanner = new Scanner(file)){
            while(scanner.hasNextLine()) {
                String line = scanner.nextLine();
                if (INPUT_METHODS_CURSOR.matcher(line).matches())
                    temp = inputMethods;
                else if (TARGET_METHODS_CURSOR.matcher(line).matches())
                    temp = targetMethods;

                if (METHOD_LINE.matcher(line).matches()) {
                    SootMethod sm = parseCastMethodSignature(line);
                    if (sm != null)
                        temp.add(sm);
                }
            }
        }catch (IOException e){
            logger.info("Error while opening log file: {}", e.getMessage());
            e.printStackTrace();
            System.exit(0);
        }
    }

    public Set<SootMethod> getInputMethods() {
        return inputMethods;
    }

    public Set<SootMethod> getTargetMethods() {
        return targetMethods;
    }

    private void reset(){
        inputMethods.clear();
        targetMethods.clear();
    }
}
