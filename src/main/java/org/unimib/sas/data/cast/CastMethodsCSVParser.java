package org.unimib.sas.data.cast;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unimib.sas.data.IDataset;
import org.unimib.sas.data.classification.ClassificationResult;
import soot.Scene;
import soot.SootMethod;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class CastMethodsCSVParser extends CastParser {
    private final static Logger logger = LoggerFactory.getLogger(CastMethodsCSVParser.class.getName());

    private File file;
    private CSVFormat format;
    private Map<String, String> sootSignatures;
    private Map<SootMethod, String> sootMethods;
    private Set<String> filtersFramework;

    public CastMethodsCSVParser(String file){
        this.file = new File(file);
        if (this.file.isDirectory() || !this.file.exists()){
            logger.error("File {} does not exists.", file);
            System.exit(0);
        }

        this.format = CSVFormat.EXCEL.withDelimiter(';').withFirstRecordAsHeader();
        this.sootSignatures = new HashMap<>();
        this.sootMethods = new HashMap<>();
        this.filtersFramework = new HashSet<>();
        this.filtersFramework.add("ejb3_persistence.jar");
        this.filtersFramework.add("javax.persistence-2.0.3.jar");
        this.filtersFramework.add("jdo2-api.jar");
        this.filtersFramework.add("javax.servlet.jsp_2.2.0.jar");
        this.filtersFramework.add("org.eclipse.swt.3.106.0.v20170608-0516.jar");
    }

    public void parse(){
        try(FileReader fr = new FileReader(file)){
            CSVParser parser = new CSVParser(fr, format);
            for(CSVRecord record : parser.getRecords()){
                String clazz = record.get("Type").replace("/","$");
                String returnType = removeSquareBrackets(record.get("ReturnType")).replace("/", "$");
                String signature = replaceConstructor(removeSquareBrackets(record.get("Signature"))).replace("=", "");
                String tag = record.get("new");
                String sootSignature = buildSootSignature(clazz, returnType, signature);
                this.sootSignatures.put(sootSignature, tag);
            }
        }catch (IOException e){
            logger.error("Error while reading file: ", file.getAbsolutePath());
            System.exit(0);
        }
        logger.info("{} methods where parsed from the csv file with cast methods.", sootSignatures.size());

    }

    public void writeToCsv(String file){
        int count = 0;
        Map<SootMethod, String> methods = getSootMethods();
        try(FileWriter fw = new FileWriter(new File(file))){
            CSVFormat format = CSVFormat.EXCEL.withDelimiter(';').withHeader(IDataset.METHOD_NAME, IDataset.TAG, "originalTag");
            CSVPrinter printer = new CSVPrinter(fw, format);
            String tag, originalTag;
            for(SootMethod sm : methods.keySet()){
                originalTag = methods.get(sm);
                if (originalTag.isEmpty())
                    continue;
                if (originalTag.equalsIgnoreCase(ClassificationResult.Input.getText()))
                    tag = ClassificationResult.Input.getText();
                else
                    tag = ClassificationResult.Target.getText();

                printer.printRecord(sm.getSignature(), tag, originalTag);
                count++;
            }
        }catch (IOException e){
            logger.error("Could not write on file {}", file);
        }
        logger.info("Written {}/{} of soot methods methods on file.", count, methods.size(), file);
    }

    public Map<String, String> getSootSignatures() {
        return sootSignatures;
    }

    public Map<SootMethod, String> getSootMethods() {
        sootMethods.clear();
        int count = 0;
        for (String signature : sootSignatures.keySet()){
            try {
                SootMethod sm = Scene.v().grabMethod(signature);
                count++;
                sootMethods.put(sm, sootSignatures.get(signature));
            }catch (RuntimeException e){
                logger.warn("Could not grab soot method {} from the Scene.", signature);
            }
        }
        logger.info("Methods retrieved from the Scene: {}/{}", count, sootSignatures.size());
        return sootMethods;
    }

    private String buildSootSignature(String clazz, String returnType, String method){
        return String.format("<%s: %s %s>", clazz, returnType, method);
    }
}
