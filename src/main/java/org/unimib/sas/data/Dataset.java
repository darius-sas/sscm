package org.unimib.sas.data;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unimib.sas.data.classification.ClassificationResult;
import org.unimib.sas.data.feature.FeatureResult;
import org.unimib.sas.utils.soot.SceneWrapper;
import soot.Scene;
import soot.SootMethod;

import java.util.*;
import java.util.function.Predicate;

/**
 * Represents the in-memory dataset extracted from the current analysis using strings.
 * Every FeatureResult is converted to an int and saved as a string.
 * Trying to retrieve a FeatureResult from a feature that does not represents a feature results will
 * return a FeatureResult.ILLEGAL value.
 */
public class Dataset implements IDataset{
    private static final Logger logger = LoggerFactory.getLogger(Dataset.class.getName());

    private List<String> header;
    private HashMap<SootMethod, HashMap<String, String>> data;

    /**
     * Creates a dataset using the features names in the IFeatureSet.
     * @param featureSet the feature set object to use.
     */
    public Dataset(IFeatureSet featureSet){
        this(featureSet.getFeatureNames());
        this.data = new HashMap<>(10000);
    }

    public Dataset(List<String> header){
        this.header = header;
        this.data = new HashMap<>(5000);
    }

    /**
     * Returns the value for the given method and the given column.
     * @param sm the method
     * @param column the column to retrieve
     * @return Rerieves a string representing the value of the given column of the given method.
     * If the column is a feature a String integer representing the FeatureResult is returned.
     * If no value is present for the given column the empty string is returned.
     */
    public String getValue(SootMethod sm, String column){
        if (data.containsKey(sm))
            return data.get(sm).getOrDefault(column, "");
        return "";
    }

    @Override
    public void reloadKeySetFromScene() {
        HashMap<SootMethod, HashMap<String, String>> newData = new HashMap<>(data.size());
        for (SootMethod sm : data.keySet()){
            try {
                SootMethod sm1 = SceneWrapper.v.grabMethod(sm.getSignature());
                if (sm1 == null)
                    throw new IllegalStateException();
                newData.put(sm, data.get(sm));
            }catch (IllegalStateException e){
                logger.error("Method {} was not found in the new scene while reloading dataset.", sm.getSignature());
            }
        }
        data.clear();
        data = newData;
    }

    /**
     * Checks if the given column is a feature column and returns the FeatureResult for the given method.
     * @param sm The method.
     * @param column The column.
     * @return The FeatureResult value of the given method or FeatureResult.ILLEGAL if the given feature was not a
     * valid feature column.
     */
    public FeatureResult getFeatureResult(SootMethod sm, String column){
        if (data.containsKey(sm) && data.get(sm).containsKey(column)){
            String value = getValue(sm, column);
            try{
                int intValue = Integer.parseInt(value);
                return FeatureResult.fromInt(intValue);
            }catch (NumberFormatException nf){
                logger.warn("The non-FeatureResult column '{}' was accessed as a FeatureResult one for the method {}.", column, sm.getName());
                return FeatureResult.ILLEGAL;
            }
        }else {
            return FeatureResult.ILLEGAL;
        }
    }

    /**
     * Adds the given FeatureResult to the given column as a String.
     * @param sm The method of the feature result.
     * @param feature The feature.
     * @param res The result.
     */
    public void setValue(SootMethod sm, String feature, FeatureResult res){
        setValue(sm, feature, res.toString());
    }

    /**
     * Adds the given value as to the given column of the given method.
     * If the column does not exists it's added. to the header.
     * @param sm The method.
     * @param column The column.
     * @param val The value to add.
     */
    public void setValue(SootMethod sm, String column, String val){
        if (!header.contains(column))
            header.add(column);

        if (!data.containsKey(sm))
            data.put(sm, new HashMap<>(header.size()));


        data.get(sm).put(column, val);
    }

    /**
     * Returns the row of the given method. If no value is present for a column an empty string is used.
     * @param sm The method
     * @return The list of values of the given method as strings. The feature result columns are represented as
     * string integers.
     */
    public List<String> getRow(SootMethod sm){
        List<String> row = new ArrayList<>();
        if (data.containsKey(sm)) {
            for (String h : header) {
                row.add(data.get(sm).getOrDefault(h, ""));
            }
        }
        return row;
    }

    public HashMap<SootMethod, HashMap<String, String>> getData() {
        return data;
    }

    public List<String> getHeader() {
        return header;
    }

    /**
     * Returns the classification of the given method using "tag" as column name
     * @param sm The method to search for its classification
     * @return The classification of the given method. Null is returned if the value is not found
     */
    public ClassificationResult getClassification(SootMethod sm){
        return getClassification(sm, "tag");
    }

    /**
     * Returns the classification of the given method using the given value as column name
     * @param sm The method to search for its classification
     * @return The classification of the given method. Null is returned if the value is not found
     */
    public ClassificationResult getClassification(SootMethod sm, String classificationColumn){
        return ClassificationResult.fromString(data.get(sm).getOrDefault(classificationColumn, null));
    }

    /**
     * Returns the classification of the given method using the given value as column name
     * @param methodSignature The method signature to search for its classification
     * @return The classification of the given method. Null is returned if the value is not found
     */
    public ClassificationResult getClassification(String methodSignature, String classificationColumn){
        return getClassification(SceneWrapper.v.grabMethod(methodSignature), classificationColumn);
    }

    /**
     * Returns the classification of the given method using "tag" column name
     * @param methodSignature The method signature to search for its classification
     * @return The classification of the given method. Null is returned if the value is not found
     */
    public ClassificationResult getClassificationResult(String methodSignature){
        return getClassification(methodSignature, "tag");
    }

    /**
     * Creates an iterable filter that iterates only over the methods that satisfy the given predicate.
     * @param p The predicate that methods must satisfy in order for them to be returned.
     * @return A filter that iterates over the dataset of methods that satisfy p.
     */
    @Override
    public Filter<IDataset, SootMethod> keep(Predicate<SootMethod> p) {
        return new Filter<>(this, p);
    }

    @Override
    public int size() {
        return data.size();
    }

    @Override
    public boolean isEmpty() {
        return data.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        if (!(o instanceof SootMethod))
            return false;
        return data.containsKey(o);
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @NotNull
    @Override
    public Iterator<SootMethod> iterator() {
        return data.keySet().iterator();
    }

    @NotNull
    @Override
    public Object[] toArray() {
        return data.keySet().toArray();
    }

    @NotNull
    @Override
    public <T> T[] toArray(@NotNull T[] a) {
        return data.keySet().toArray(a);
    }

    @Override
    public boolean add(SootMethod sootMethod) {
        throw new UnsupportedOperationException("Operation not supported, use setValue().");
    }

    @Override
    public boolean remove(Object o) {
        return data.remove(o) != null;
    }

    @Override
    public boolean containsAll(@NotNull Collection<?> c) {
        return data.keySet().containsAll(c);
    }

    @Override
    public boolean addAll(@NotNull Collection<? extends SootMethod> c) {
        throw new UnsupportedOperationException("Operation not supported, iterate over the given parameter and use setValue().");
    }

    @Override
    public boolean removeAll(@NotNull Collection<?> c) {
        throw new UnsupportedOperationException("Operation not supported, use remove().");
    }

    @Override
    public boolean retainAll(@NotNull Collection<?> c) {
        throw new UnsupportedOperationException("Operation not supported.");
    }

    @Override
    public void clear() {
        data.clear();
    }

}
