package org.unimib.sas.data;

import soot.SootMethod;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

/**
 * Models the main dataset structure where to save soot methods and their tags.
 */
public interface IDataset extends Iterable<SootMethod>, Collection<SootMethod>{

    final String TAG = "tag";
    final String PREDICTED = "predictedTag";
    final String METHOD_NAME = "methodName";
    final String WINNER_PROB = "winnerProb";

    /**
     * Returns the value of the given column for the given name.
     * @param sm The method object.
     * @param colName The name of the column.
     * @return The string representing the value at the given column of the given method.
     */
    String getValue(SootMethod sm, String colName);

    /**
     * Useful if the Scene has changed and you want to reload the new methods objects from the Scene.
     */
    void reloadKeySetFromScene();

    /**
     * Sets the new value to the given method for the given column.
     * @param sm The method.
     * @param colName The name of the column.
     * @param value The value to set to the given method on the given column.
     */
    void setValue(SootMethod sm, String colName, String value);

    /**
     * Returns the whole row of the given method.
     * @param sm The method.
     * @return A list of strings (the values) ordered according to the header.
     */
    List<String> getRow(SootMethod sm);

    /**
     * The header of the current dataset.
     * @return The header of the current dataset.
     */
    List<String> getHeader();

    /**
     * Iterates over the dataset over only the methods that evaluates to true the given predicate.
     * To be used in for each constructs.
     * @param p The predicate, if True the method will be returned.
     * @return A filter iterable over only the methods that evaluates to true the given predicate.
     */
    Filter<IDataset, SootMethod> keep(Predicate<SootMethod> p);
}
