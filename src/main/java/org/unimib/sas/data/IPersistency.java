package org.unimib.sas.data;

import java.io.File;

/**
 * Models a persistency of a IDataset object
 */
public interface IPersistency {
    void save(IDataset data);
    IDataset load();
    File getFile();
}
