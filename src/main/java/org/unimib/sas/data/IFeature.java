package org.unimib.sas.data;

import org.unimib.sas.data.feature.FeatureResult;
import soot.SootMethod;

/**
 * Represents a stateless feature with a given name.
 * The apply() method calculates the feature results for the given SootMethod.
 */
public interface IFeature {

    /**
     * Sets the name of the feature. This name will be used to index the column of the IDataset.
     * @param name the name to use
     */
    void setFeatureName(String name);

    /**
     * Returns the name of the feature.
     * @return the name of the feature.
     */
    String getFeatureName();

    /**
     * Calculates the feature value on the given method.
     * @param sm The method under analysis.
     * @return The result of the analysis.
     */
    FeatureResult apply(SootMethod sm);

}
