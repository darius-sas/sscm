package org.unimib.sas.data;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unimib.sas.utils.soot.SceneWrapper;
import soot.Scene;
import soot.SootMethod;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Represents a CSV dataset state, allowing to save/load a IDataset to/from a CSV file.
 */
public class CSVPersistency implements IPersistency {

    private static final Logger logger = LoggerFactory.getLogger(CSVPersistency.class.getName());

    private File workingFile;
    private CSVFormat format;

    public CSVPersistency(String workingFile){
        this.workingFile = new File(workingFile);
        if (this.workingFile.isDirectory())
            throw new IllegalArgumentException("workingFile parameter must be a file not a directory.");
        try {
            this.workingFile.createNewFile();
        } catch (IOException e) {
            logger.error("Could not create working file {} because {}.", workingFile, e.getMessage());
            System.exit(0);
        }
        format = CSVFormat.EXCEL.withDelimiter(';');
    }

    @Override
    public void save(IDataset data) {
        List<String> header = data.getHeader();
        if (!header.contains("methodName"))
            header.add(0, "methodName");
        boolean hasTag = header.contains("tag");
        if (!hasTag)
            header.add(1, "tag");
        format = format.withHeader(header.toArray(new String[]{}));
        try {
            FileWriter fw = new FileWriter(workingFile);
            CSVPrinter printer = new CSVPrinter(fw, format);
            for (SootMethod sm : data){
                List<String> row = data.getRow(sm);
                row.set(0, sm.getSignature());
                printer.printRecord(row);
            }
            printer.flush();
            printer.close();
        }catch (IOException e){
            logger.error("Could not write on {}, caused by: {}", workingFile.getName(), e.getMessage());
        }
    }

    @Override
    public IDataset load() {
        format = format.withFirstRecordAsHeader();
        try{
            FileReader fr = new FileReader(workingFile);
            CSVParser parser = new CSVParser(fr, format);
            Map<String, Integer> headerMap = parser.getHeaderMap();
            List<String> header = new ArrayList<>(headerMap.values().size());
            for(Map.Entry<String, Integer> e : headerMap.entrySet()){
                header.add(e.getValue(), e.getKey());
            }
            Dataset dataset = new Dataset(header);

            for (CSVRecord record : parser.getRecords()){
                try {
                    SootMethod sm = SceneWrapper.v.grabMethod(record.get(0));
                    for (String h : header){
                        dataset.setValue(sm, h, record.get(h));
                    }
                }catch (RuntimeException e){
                    logger.warn("Could not add to dataset the method {} because was not present in the soot Scene", e.getMessage().substring(e.getMessage().indexOf('<')));
                }
            }
            if (dataset.getData().keySet().isEmpty())
                logger.error("Could not load any method. Dataset is empty.");
            parser.close();
            return dataset;
        }catch (IOException e){
            logger.error("Could not read data from {}, caused by: {}", workingFile.getName(), e.getMessage());
        }
        return null;
    }

    @Override
    public File getFile() {
        return workingFile;
    }

}
