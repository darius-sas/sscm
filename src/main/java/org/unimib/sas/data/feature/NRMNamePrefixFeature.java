package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;
import soot.SootMethod;
import soot.Unit;
import soot.jimple.Stmt;

/**
 * A feature that checks if the given method invokes a NRM that starts with the given prefix.
 */
public class NRMNamePrefixFeature extends NamePrefixFeature{
    /**
     * Creates a feature that checks if the given method invokes a NRM that starts with the given prefix.
     * @param featureName The name of the feature
     * @param nrManager The ResourceManagerType instance
     * @param prefix the prefix to check for.
     */
    public NRMNamePrefixFeature(String featureName, IResourceManager nrManager, String prefix) {
        super(featureName, nrManager, prefix);
    }

    @Override
    public FeatureResult apply(SootMethod sm) {
        try {
            if (!isValidSootMethod(sm) || !sm.isConcrete() || sm.retrieveActiveBody() == null)
                return FeatureResult.UNDEFINED;
        }catch (RuntimeException e){
            if (e.getCause() instanceof NullPointerException)
                return FeatureResult.UNDEFINED;
            else
                throw e;
        }
        // Iterate over the body and find an invoke expression to a NRM
        // then check if the prefix matches
        for (Unit u : sm.retrieveActiveBody().getUnits()) {
            try {
                if (u instanceof Stmt) {
                    Stmt stmt = (Stmt) u;
                    if (stmt.containsInvokeExpr()) {
                        SootMethod nrm = stmt.getInvokeExpr().getMethod();
                        if (getNrManager().isNativeResourceMethod(nrm)
                                && nrm.getName().startsWith(prefix)) {
                            return FeatureResult.TRUE;
                        }
                    }
                }
            } catch (soot.ResolutionFailedException e) {
                continue;
            }
        }
        return FeatureResult.FALSE;
    }
}
