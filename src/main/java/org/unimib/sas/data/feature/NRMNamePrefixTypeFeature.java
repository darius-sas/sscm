package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;
import org.unimib.sas.utils.Prefixes;
import soot.SootMethod;
import soot.Unit;
import soot.jimple.Stmt;

import java.util.Collection;

/**
 * Checks if the given method invokes a NRM that starts with the given prefixes.
 */
public class NRMNamePrefixTypeFeature extends AbstractFeature {

    private Collection<String> prefixes;

    public NRMNamePrefixTypeFeature(String featureName, IResourceManager nrManager, Collection<String> prefixes) {
        super(featureName, nrManager);
        this.prefixes = prefixes;
    }

    @Override
    public FeatureResult apply(SootMethod sm) {
        try {
            if (!isValidSootMethod(sm) || !sm.isConcrete() || sm.retrieveActiveBody() == null)
                return FeatureResult.UNDEFINED;
        }catch (RuntimeException e){
            if (e.getCause() instanceof NullPointerException)
                return FeatureResult.UNDEFINED;
            else
                throw e;
        }
        // Iterate over the body and find an invoke expression to a NRM
        // then check if the prefix matches
        for (Unit u : sm.retrieveActiveBody().getUnits()) {
            try {
                if (u instanceof Stmt) {
                    Stmt stmt = (Stmt) u;
                    if (stmt.containsInvokeExpr()) {
                        SootMethod nrm = stmt.getInvokeExpr().getMethod();
                        if (getNrManager().isNativeResourceMethod(nrm)
                                && Prefixes.startsWithAny(nrm.getName(), prefixes)) {
                            return FeatureResult.TRUE;
                        }
                    }
                }
            } catch (soot.ResolutionFailedException e) {
                continue;
            }
        }
        return FeatureResult.FALSE;
    }
}
