package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;
import soot.SootClass;
import soot.SootMethod;

import java.util.function.Predicate;

/**
 * Checks if the given soot method or his declaring class respect the given predicates.
 * null predicates are ignored. If both null, {@link FeatureResult}.UNDEFINED is returned.
 */
public class PredicateFeature extends AbstractFeature {

    private Predicate<SootMethod> methodPredicate;
    private Predicate<SootClass> classPredicate;

    public PredicateFeature(String featureName, IResourceManager nrManager, Predicate<SootClass> classPredicate, Predicate<SootMethod> methodPredicate) {
        super(featureName, nrManager);
        this.classPredicate = classPredicate;
        this.methodPredicate = methodPredicate;
    }


    @Override
    public FeatureResult apply(SootMethod sm) {
        if (!isValidSootMethod(sm) || (methodPredicate == null && classPredicate == null))
            return FeatureResult.UNDEFINED;
        else if (methodPredicate == null)
            return classPredicate.test(sm.getDeclaringClass()) ? FeatureResult.TRUE : FeatureResult.FALSE;
        else if (classPredicate == null)
            return methodPredicate.test(sm) ? FeatureResult.TRUE : FeatureResult.FALSE;
        else
            return methodPredicate.test(sm) && classPredicate.test(sm.getDeclaringClass()) ?
            FeatureResult.TRUE : FeatureResult.FALSE;
    }
}
