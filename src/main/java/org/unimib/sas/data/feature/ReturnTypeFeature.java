package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;
import soot.Scene;
import soot.SootMethod;
import soot.Type;

/**
 * Models a feature that checks if the return type of the given method matches the given type or is a child.
 */
public class ReturnTypeFeature extends AbstractFeature {

    private Type sootType;
    private boolean includeChildren;

    /**
     * Instantiates a new ReturnTypeFeature.
     * @param featureName The name of the feature
     * @param nrManager The native resource manager to use.
     * @param type the soot type to check
     * @param includeChildren whether to include children in the check
     */
    public ReturnTypeFeature(String featureName, IResourceManager nrManager, Type type, boolean includeChildren) {
        super(featureName, nrManager);
        this.sootType = type;
        this.includeChildren = includeChildren;
    }

    @Override
    public FeatureResult apply(SootMethod sm) {
        if (!isValidSootMethod(sm))
            return FeatureResult.UNDEFINED;
        if (sm.getReturnType().equals(sootType) || isChildOf(sm.getReturnType(), sootType))
            return FeatureResult.TRUE;
        return FeatureResult.FALSE;
    }

    private boolean isChildOf(Type child, Type parent){
        return includeChildren &&  Scene.v().getOrMakeFastHierarchy().canStoreType(child, parent);
    }
}
