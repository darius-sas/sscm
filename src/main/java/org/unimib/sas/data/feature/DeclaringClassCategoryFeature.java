package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;
import org.unimib.sas.data.nrm.PrefixResourcesManager;
import soot.SootClass;
import soot.SootMethod;

/**
 * Checks if the declaring type of the current method is of the given {@link IResourceManager.Category}.
 */
public class DeclaringClassCategoryFeature extends AbstractFeature {

    private String category;

    public DeclaringClassCategoryFeature(String featureName, IResourceManager nrManager, String category) {
        super(featureName, nrManager);
        this.category = category;
    }

    public DeclaringClassCategoryFeature(String featureName, PrefixResourcesManager nrManager, IResourceManager.Category category) {
        this(featureName, nrManager, category.toString());
    }

    @Override
    public FeatureResult apply(SootMethod sm) {
        if (!isValidSootMethod(sm))
            return FeatureResult.UNDEFINED;

        SootClass nrc = getNrManager().isNativeResourceClass(sm.getDeclaringClass());
        if (nrc == null)
            return FeatureResult.FALSE;
        return getNrManager().getNativeResourceCategory(nrc).equalsIgnoreCase(category) ? FeatureResult.TRUE : FeatureResult.FALSE;
    }
}
