package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;
import soot.SootMethod;

public class HasParametersFeature extends ParametersFeature {

    private int paramThreshold;

    public HasParametersFeature(String featureName, IResourceManager nrManager, int paramThreshold) {
        super(featureName, nrManager);
        this.paramThreshold = paramThreshold;
    }

    @Override
    public FeatureResult apply(SootMethod sm) {
        if (!isValidSootMethod(sm))
            return FeatureResult.UNDEFINED;
        else
            return sm.getParameterCount() > paramThreshold ? FeatureResult.TRUE : FeatureResult.FALSE;
    }
}
