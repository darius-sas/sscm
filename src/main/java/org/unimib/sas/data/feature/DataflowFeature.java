package org.unimib.sas.data.feature;

import org.jetbrains.annotations.NotNull;
import org.unimib.sas.data.IResourceManager;
import org.unimib.sas.tdfa.TaintAnalysis;
import soot.Body;
import soot.SootMethod;
import soot.Unit;
import soot.jimple.InvokeExpr;
import soot.jimple.Stmt;

import java.util.HashSet;

public abstract class DataflowFeature extends AbstractFeature{

    private static final TaintAnalysis taintAnalyzer = new TaintAnalysis();

    public DataflowFeature(String featureName, IResourceManager nrManager) {
        super(featureName, nrManager);
    }


    protected TaintAnalysis getTaintAnalyzer(){return taintAnalyzer;}

    @NotNull
    protected HashSet<InvokeExpr> extractNativeResourceMethodStatements(Body body) {
        HashSet<InvokeExpr> nrmStatements = new HashSet<>();
        for (Unit u : body.getUnits()){
            try {
                Stmt stmt = (Stmt) u;
                if (stmt.containsInvokeExpr()) {
                    SootMethod invokedSootMethod = stmt.getInvokeExpr().getMethod();
                    if (getNrManager().isNativeResourceMethod(invokedSootMethod)) {
                        nrmStatements.add(stmt.getInvokeExpr());
                    }
                }
            }catch (soot.ResolutionFailedException e){
                continue;
            }
        }
        return nrmStatements;
    }

    public boolean isValidSootMethod(SootMethod sm){
        return super.isValidSootMethod(sm) && sm.isConcrete() && sm.hasActiveBody();
    }
}
