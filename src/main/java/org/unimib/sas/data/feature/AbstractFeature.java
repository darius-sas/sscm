package org.unimib.sas.data.feature;

import org.unimib.sas.data.IFeature;
import org.unimib.sas.data.IResourceManager;
import soot.SootMethod;

public abstract class AbstractFeature implements IFeature {
    private String featureName;
    private IResourceManager nrManager;

    public AbstractFeature(String featureName, IResourceManager nrManager) {
        this.featureName = featureName;
        this.nrManager = nrManager;
    }

    @Override
    public String getFeatureName() {
        return featureName;
    }

    @Override
    public void setFeatureName(String featureName) {
        this.featureName = featureName;
    }

    public boolean isValidSootMethod(SootMethod sm){
        return !sm.isPhantom();
    }

    public IResourceManager getNrManager() {
        return nrManager;
    }

}
