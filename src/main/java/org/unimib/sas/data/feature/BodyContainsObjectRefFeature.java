package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;
import soot.SootMethod;

public class BodyContainsObjectRefFeature extends NameContainsFeature {

    public BodyContainsObjectRefFeature(String featureName, IResourceManager nrManager, String pattern) {
        super(featureName, nrManager, pattern);
    }

    @Override
    public FeatureResult apply(SootMethod sm) {
        if (!isValidSootMethod(sm))
            return FeatureResult.UNDEFINED;
        if (sm.retrieveActiveBody().toString().toLowerCase().contains(pattern))
            return FeatureResult.TRUE;
        return FeatureResult.FALSE;
    }

}
