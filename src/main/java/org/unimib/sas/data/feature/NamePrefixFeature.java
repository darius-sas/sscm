package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;

public abstract class NamePrefixFeature extends AbstractFeature {

    protected String prefix;

    public NamePrefixFeature(String featureName, IResourceManager nrManager, String prefix) {
        super(featureName, nrManager);
        this.prefix = prefix;
    }


    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
}
