package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;
import soot.*;
import soot.jimple.InvokeExpr;

import java.util.HashMap;
import java.util.HashSet;

public class DataflowFromNRMtoParam extends DataflowFeature {
    public DataflowFromNRMtoParam(String featureName, IResourceManager nrManager) {
        super(featureName, nrManager);
    }

    @Override
    public FeatureResult apply(SootMethod sm) {
        if (!isValidSootMethod(sm))
            return FeatureResult.UNDEFINED;

        Body body = sm.retrieveActiveBody();
        HashSet<InvokeExpr> nrmStmts = extractNativeResourceMethodStatements(body);
        HashMap<Unit, HashSet<Value>> taintOut = getTaintAnalyzer().taintAnalysisMethodCalls(body, nrmStmts);

        // Iterate over the body units and check whether there is a tainted parameter flowing out.
        for (Unit u : body.getUnits()){
            for (Local p : body.getParameterLocals())
                if (p.getType() instanceof  RefLikeType && taintOut.get(u).contains(p))
                    return FeatureResult.TRUE;
        }

        return FeatureResult.FALSE;
    }
}
