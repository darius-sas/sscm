package org.unimib.sas.data.feature;

import org.unimib.sas.data.IResourceManager;
import soot.PrimType;
import soot.SootMethod;
import soot.Type;

/**
 * Checks whether the method has a primitive parameter
 */
public class PrimitiveParametersFeature extends ParametersFeature {

    public PrimitiveParametersFeature(String featureName, IResourceManager nrManager) {
        super(featureName, nrManager);
    }

    @Override
    public FeatureResult apply(SootMethod sm) {
        if (!isValidSootMethod(sm) || sm.getParameterCount() == 0)
            return FeatureResult.UNDEFINED;

        boolean result = false;

        for (Type t : sm.getParameterTypes()){
            result = t instanceof PrimType;
            if (result)
                break;
        }

        return result ? FeatureResult.TRUE : FeatureResult.FALSE;
    }
}
