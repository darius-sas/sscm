package org.unimib.sas.data;

import soot.SootClass;

import java.util.Collection;

/**
 * Models a dataset generator
 * @param <T> type of the data structure to use.
 */
public interface IDatasetGenerator<T> {

    /**
     * Generates a dataset from the given soot classes.
     * @param sootClasses the classes for which to generate a dataset.
     */
    void generate(Collection<SootClass> sootClasses);

    /**
     * Generates a dataset using the given data.
     * @param methodsDataset the data on which to calculate the dataset
     */
    void generate(T methodsDataset);

    /**
     * Retrieves the generated dataset. This value might be null if the dataset was not calculated yet.
     * @return the dataset, or null if it was not calculated yet.
     */
    T getDataset();

}
