package org.unimib.sas.data.featureset;

import org.unimib.sas.data.IFeature;
import org.unimib.sas.data.IFeatureSet;
import org.unimib.sas.data.IResourceManager;
import org.unimib.sas.data.feature.*;
import soot.RefType;
import soot.SootClass;
import soot.Type;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

/**
 * Models an abstract FeatureSet used to create features functions.
 */
public abstract class AbstractFeatureSet implements IFeatureSet {

    protected IResourceManager nativeResourcesManager;
    protected List<IFeature> featureList;

    public AbstractFeatureSet(IResourceManager nativeResourcesManager){
        this.nativeResourcesManager = nativeResourcesManager;
        featureList = new LinkedList<>();
    }


    /**
     * Returns all the names of the features in the same order of the
     * underlying feature list (the same order of calculation).
     *
     * @return A list of strings, each is the name of a feature.
     */
    @Override
    public List<String> getFeatureNames() {
        List<String> names = new ArrayList<>();
        for (IFeature f : featureList){
            names.add(f.getFeatureName());
        }
        return names;
    }


    /**
     * Creates and adds to the current set of features method name prefix features using the given prefixes and the name suffix.
     * @param prefixes The prefixes to use to create the features
     * @param featureNameSuffix The suffix to use for the feature names.
     */
    protected void createMethodNamePrefixFeatures(Collection<String> prefixes, String featureNameSuffix){
        for (String prefix : prefixes){
            String fName = prefix + featureNameSuffix;
            IFeature f = new MethodNamePrefixFeature(fName, nativeResourcesManager, prefix);
            featureList.add(f);
        }
    }

    /**
     * Creates and adds to the feature list the features with class name patterns.
     * @param patterns The patterns to search for.
     * @param featureNameSuffix The suffix to use for feature names.
     */
    protected void createClassNameContainsFeatures(Collection<String> patterns, String featureNameSuffix){
        for (String pattern : patterns){
            String fName = pattern + featureNameSuffix;
            IFeature f = new ClassNameContainsFeature(fName, nativeResourcesManager, pattern);
            featureList.add(f);
        }
    }

    /**
     * Creates and adds to the feature list the type parameter features using the name of the class as prefix
     * and the given suffix as suffix.
     * @param classes The classes to add as type parameter feature.
     * @param featureNameSuffix The suffix of the feature names.
     * @param includeChildren Whether to include children to the type matching.
     */
    protected void createTypeParametersFeatures(Collection<String> classes, String featureNameSuffix, boolean includeChildren){
        for (String c : classes){
            Type t = RefType.v(c);
            String fName = t.toString().substring(t.toString().lastIndexOf('.')+1) + featureNameSuffix;
            IFeature f = new TypeParametersFeature(fName, nativeResourcesManager, t, includeChildren);
            featureList.add(f);
        }
    }

    /**
     * Creates NRM prefix features that check wheter there is a call to a NRM inside the method body that starts with
     * the given prefixes
     * @param patterns The prefixes
     * @param featureNameSuffix The suffix name of the feature
     */
    protected void createNRMNamePrefixFeatures(Collection<String> patterns, String featureNameSuffix){
        for (String c : patterns){
            String fName = c + featureNameSuffix;
            IFeature f = new NRMNamePrefixFeature(fName, nativeResourcesManager, c);
            featureList.add(f);
        }
    }

    /**
     * Adds to the feature list a new {@link DeclaringClassIsSubtypeOfFeature} object for each soot class in classes.
     * @param classes The classes to use for building the {@link DeclaringClassIsSubtypeOfFeature}.
     * @param featureNameSuffix The suffix of the features.
     */
    protected void createDeclaringClassIsSubtypeOfFeatures(Collection<SootClass> classes, String featureNameSuffix){
        for (SootClass sc : classes){
            String fName = sc.getShortName() + featureNameSuffix;
            IFeature f = new DeclaringClassIsSubtypeOfFeature(fName, nativeResourcesManager, sc);
            featureList.add(f);
        }
    }
}
