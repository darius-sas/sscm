package org.unimib.sas.data.featureset;

import org.unimib.sas.data.IFeature;
import org.unimib.sas.data.IResourceManager;
import org.unimib.sas.data.feature.*;
import org.unimib.sas.data.nrm.PrefixesResourceConfig;
import org.unimib.sas.data.nrm.PrefixResourcesManager;
import org.unimib.sas.utils.Prefixes;
import soot.*;

import java.util.*;

/**
 * Generates a features list by aggregating some types of features into three categories.
 */
public class AggregatedFeatureSet extends AbstractFeatureSet {
    private PrefixResourcesManager nativeResourcesManager;

    public AggregatedFeatureSet(PrefixResourcesManager nativeResourcesManager) {
        super(nativeResourcesManager);
        this.nativeResourcesManager = nativeResourcesManager;
    }

    @Override
    public List<IFeature> getFeatureList() {
        if (!featureList.isEmpty()){
            return Collections.unmodifiableList(featureList);
        }
        PrefixesResourceConfig prefixes = nativeResourcesManager.getPrefixes();

        // Class name contains certain keywords
        createClassNameContainsFeatures(prefixes.getClassNamePatterns(), "ClassNameContains");

        // Method has more than 0 params
        featureList.add(new HasParametersFeature("HasMore0ParametersFeature", nativeResourcesManager, 0));

        // Parameters are of given type or of its subtypes.
        featureList.add(new TypeParametersFeature("inputNRCParamType", nativeResourcesManager, RefType.v(nativeResourcesManager.getDummyInputNativeResourceInterface())));
        featureList.add(new TypeParametersFeature("targetNRCParamType", nativeResourcesManager, RefType.v(nativeResourcesManager.getDummyTargetNativeResourceInterface())));
        featureList.add(new TypeParametersFeature("mixedNRCParamType", nativeResourcesManager, RefType.v(nativeResourcesManager.getDummyMixedNativeResourceInterface())));
        featureList.add(new TypeParametersFeature("StringParam", nativeResourcesManager, RefType.v("java.lang.String")));
        featureList.add(new TypeParametersFeature("String[]Param", nativeResourcesManager, RefType.v("java.lang.String").makeArrayType()));
        featureList.add(new TypeParametersFeature("Byte[]Param", nativeResourcesManager, ArrayType.v(ByteType.v(), 1)));
        featureList.add(new TypeParametersFeature("Char[]Param", nativeResourcesManager, ArrayType.v(CharType.v(), 1)));
        featureList.add(new PrimitiveParametersFeature("primitiveParam", nativeResourcesManager));

        // Return type features
        List<Map.Entry<String, Type>> retTypes = new ArrayList<>();
        retTypes.add(new AbstractMap.SimpleEntry<>("StringReturnType", RefType.v("java.lang.String")));
        retTypes.add(new AbstractMap.SimpleEntry<>("VoidReturnType", VoidType.v()));
        retTypes.add(new AbstractMap.SimpleEntry<>("String[]ReturnType", ArrayType.v(RefType.v("java.lang.String"),1)));
        retTypes.add(new AbstractMap.SimpleEntry<>("Byte[]ReturnType", ArrayType.v(ByteType.v(), 1)));
        retTypes.add(new AbstractMap.SimpleEntry<>("ByteReturnType", ByteType.v()));
        for (Map.Entry<String, Type> e : retTypes){
            featureList.add(new ReturnTypeFeature(e.getKey(), nativeResourcesManager, e.getValue(), false));
        }
        // Return type of native resource?
        featureList.add(new ReturnTypeFeature(
                "NativeResReturnType",
                nativeResourcesManager,
                RefType.v(nativeResourcesManager.getDummyNativeResourceInterface()), true));

        // Return type is primitive
        featureList.add(new PrimitiveReturnTypeFeature("primitiveReturnType", nativeResourcesManager));

        // Invokes a NRM that starts with the given prefixes?
        featureList.add(new NRMNamePrefixTypeFeature("invokesInputNRMPrefix", nativeResourcesManager, prefixes.getAllInputMethodPrefixes()));
        featureList.add(new NRMNamePrefixTypeFeature("invokesTargetNRMPrefix", nativeResourcesManager, prefixes.getAllTargetMethodPrefixes()));
        featureList.add(new NRMNamePrefixTypeFeature("invokesMixedNRMPrefix", nativeResourcesManager, prefixes.getAllMixedMethodPrefixes()));


        // Inter-procedural dataflow features
        featureList.add(new DataflowFromNRMToThis("DFfromNRMtoThis", nativeResourcesManager));
        featureList.add(new DataflowFromNRMToReturn("DFfromNRMtoRet", nativeResourcesManager));
        featureList.add(new DataflowFromParamToNRM("DFfromParamToNRM", nativeResourcesManager));
        featureList.add(new DataflowFromParamToThis("DFfromParamToThis", nativeResourcesManager));
        featureList.add(new DataflowFromNRMtoParam("DFfromNRMtoParam", nativeResourcesManager));

        // Declaring class extends/implements a Native Resource Class
        createDeclaringClassIsSubtypeOfFeatures(new ArrayList<>(nativeResourcesManager.getSootNativeClasses()), "SubtypeOfNRC");

        // Declaring class is of PrefixResourcesManager.Category type
        featureList.add(new DeclaringClassCategoryFeature("inputNRCategory", nativeResourcesManager, IResourceManager.Category.Input));
        featureList.add(new DeclaringClassCategoryFeature("targetNRCategory", nativeResourcesManager, IResourceManager.Category.Target));
        featureList.add(new DeclaringClassCategoryFeature("mixedNRCategory", nativeResourcesManager, IResourceManager.Category.Mixed));

        // Return Type is of PrefixResourcesManager.Category
        featureList.add(new ReturnTypeCategoryFeature("inputNRretType", nativeResourcesManager, IResourceManager.Category.Input));
        featureList.add(new ReturnTypeCategoryFeature("targetNRretType", nativeResourcesManager, IResourceManager.Category.Target));
        featureList.add(new ReturnTypeCategoryFeature("mixedNRretType", nativeResourcesManager, IResourceManager.Category.Mixed));

        // Method is private/public/concrete
        //featureList.add(new PredicateFeature("privateMethod", nativeResourcesManager,null, SootMethod::isPrivate)); // Questi modificatori non dovrebbero
        //featureList.add(new PredicateFeature("publicMethod", nativeResourcesManager, null, SootMethod::isPublic)); // influenzare la classificazione...
        featureList.add(new PredicateFeature("concreteMethod", nativeResourcesManager, null, SootMethod::isConcrete));

        // Declaring class is interface or abstract class or concrete class
        featureList.add(new PredicateFeature("isInterface", nativeResourcesManager, SootClass::isInterface, null));
        featureList.add(new PredicateFeature("isAbstractClass", nativeResourcesManager, SootClass::isAbstract, null));
        featureList.add(new PredicateFeature("isConcreteClass", nativeResourcesManager, SootClass::isConcrete, null));

        // Prefix is of type input/target/mixed
        featureList.add(new PredicateFeature("inputPrefixType", nativeResourcesManager, null,
                x-> Prefixes.startsWithAny(x.getName(), prefixes.getAllInputMethodPrefixes())));
        featureList.add(new PredicateFeature("targetPrefixTye", nativeResourcesManager, null,
                x-> Prefixes.startsWithAny(x.getName(), prefixes.getAllTargetMethodPrefixes())));
        featureList.add(new PredicateFeature("mixedPrefixType", nativeResourcesManager, null,
                x-> Prefixes.startsWithAny(x.getName(), prefixes.getAllMixedMethodPrefixes())));

        return featureList;
    }
}
