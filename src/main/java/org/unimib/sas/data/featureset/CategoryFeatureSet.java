package org.unimib.sas.data.featureset;

import org.unimib.sas.data.IFeature;
import org.unimib.sas.data.feature.*;
import org.unimib.sas.data.nrm.CategoryResourceManager;
import org.unimib.sas.data.nrm.CategoryResourceConfig;
import org.unimib.sas.utils.Prefixes;
import soot.*;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Implements a feature set whose feature are based on categories of Resources.
 * It is the most generalizing feature set and thus is the preferable one to use.
 */
public class CategoryFeatureSet extends AbstractFeatureSet {

    private CategoryResourceManager nativeResourcesManager;

    public CategoryFeatureSet(CategoryResourceManager nativeResourcesManager) {
        super(nativeResourcesManager);
        this.nativeResourcesManager = nativeResourcesManager;
    }

    /**
     * Returns an ordered list of the features.
     *
     * @return A list where each item is an initialiazed IFeature and ready to be used to compute the feature for
     * the given method.
     */
    @Override
    public List<IFeature> getFeatureList() {
        if (!featureList.isEmpty())
            return Collections.unmodifiableList(featureList);

        CategoryResourceConfig prefixes = CategoryResourceConfig.getInstance();

        // Prefixes features
        createMethodNamePrefixFeatures(prefixes.getInputMethodPrefixes(), "InputPrefixFeature");
        createMethodNamePrefixFeatures(prefixes.getMixedMethodPrefixes(), "MixedPrefixFeature");
        createMethodNamePrefixFeatures(prefixes.getTargetMethodPrefixes(), "TargetPrefixFeature");

        // Invoked NRM features
        createNRMNamePrefixFeatures(prefixes.getAllInputMethodPrefixes(), "InputNRMInvokes");
        createNRMNamePrefixFeatures(prefixes.getAllMixedMethodPrefixes(), "MixedNRMInvokes");
        createNRMNamePrefixFeatures(prefixes.getAllTargetMethodPrefixes(), "TargetNRMInvokes");

        // Category based features
        List<String> categories = nativeResourcesManager.getCategories();
        Map<String, SootClass> categoryInterfaces = nativeResourcesManager.getCategoryInterface();
        for (String category : categories){
            // Declaring class belongs to a specific class category
            featureList.add(new DeclaringClassCategoryFeature(category + "NRCategoryFeature", nativeResourcesManager, category));

            // A method's parameter is of a certain category or a child of it.
            Type categorySootType = RefType.v(categoryInterfaces.get(category));
            featureList.add(new TypeParametersFeature(category + "ParamNRCategoryFeature", nativeResourcesManager, categorySootType, true));

            // The return type is of a certain category or a child of it
            featureList.add(new ReturnTypeFeature(category+"ReturnNRCategoryFeature", nativeResourcesManager, categorySootType, true));
        }

        // Parameters are of certain types
        featureList.add(new TypeParametersFeature("StringParam", nativeResourcesManager, RefType.v("java.lang.String")));
        featureList.add(new TypeParametersFeature("String[]Param", nativeResourcesManager, RefType.v("java.lang.String").makeArrayType()));
        featureList.add(new TypeParametersFeature("Byte[]Param", nativeResourcesManager, ArrayType.v(ByteType.v(), 1)));
        featureList.add(new TypeParametersFeature("Char[]Param", nativeResourcesManager, ArrayType.v(CharType.v(), 1)));
        featureList.add(new PrimitiveParametersFeature("primitiveParam", nativeResourcesManager));

        // Dataflow features
        featureList.add(new DataflowFromNRMToThis("DFfromNRMtoThis", nativeResourcesManager));
        featureList.add(new DataflowFromNRMToReturn("DFfromNRMtoRet", nativeResourcesManager));
        featureList.add(new DataflowFromParamToNRM("DFfromParamToNRM", nativeResourcesManager));
        featureList.add(new DataflowFromParamToThis("DFfromParamToThis", nativeResourcesManager));
        featureList.add(new DataflowFromNRMtoParam("DFfromNRMtoParam", nativeResourcesManager));

        // The method is concrete or not
        featureList.add(new PredicateFeature("concreteMethod", nativeResourcesManager, null, SootMethod::isConcrete));

        // Declaring class is interface or abstract class or concrete class
        featureList.add(new PredicateFeature("isInterface", nativeResourcesManager, SootClass::isInterface, null));
        featureList.add(new PredicateFeature("isAbstractClass", nativeResourcesManager, SootClass::isAbstract, null));
        featureList.add(new PredicateFeature("isConcreteClass", nativeResourcesManager, SootClass::isConcrete, null));

        // The prefix of the method is of a certain type
        featureList.add(new PredicateFeature("inputPrefixType", nativeResourcesManager, null,
                x-> Prefixes.startsWithAny(x.getName(), prefixes.getAllInputMethodPrefixes())));
        featureList.add(new PredicateFeature("targetPrefixTye", nativeResourcesManager, null,
                x-> Prefixes.startsWithAny(x.getName(), prefixes.getAllTargetMethodPrefixes())));
        featureList.add(new PredicateFeature("mixedPrefixType", nativeResourcesManager, null,
                x-> Prefixes.startsWithAny(x.getName(), prefixes.getAllMixedMethodPrefixes())));

        // Class name contains a pattern of a certain category
        Map<String, List<String>> patternCategories = prefixes.getClassNamePatternsCategories();
        for (String patternCategory : patternCategories.keySet()){
            featureList.add(new PredicateFeature(patternCategory+"ClassNamePatternCat", nativeResourcesManager,
                    sc -> patternCategories.get(patternCategory).stream().anyMatch(x-> sc.getName().toLowerCase().contains(x.toLowerCase())),
                    null));
        }

        return Collections.unmodifiableList(featureList);
    }
}
