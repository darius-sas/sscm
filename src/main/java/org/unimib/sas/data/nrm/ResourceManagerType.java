package org.unimib.sas.data.nrm;

public enum ResourceManagerType {
    Normal("normal"),
    Category("category");

    private String value;
    ResourceManagerType(String value){
        this.value = value;
    }

    public static ResourceManagerType fromString(String string){
        for (ResourceManagerType nrm : ResourceManagerType.values()){
            if (nrm.value.equalsIgnoreCase(string))
                return nrm;
        }
        throw new IllegalArgumentException("The given string is not a valid ResourceManagerType.");
    }
}
