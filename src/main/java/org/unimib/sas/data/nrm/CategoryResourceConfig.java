package org.unimib.sas.data.nrm;

import com.esotericsoftware.yamlbeans.YamlReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Category resource configuration model. Reads the given YAML file and loads the configuration
 * from it into the object.
 */
public class CategoryResourceConfig extends AbstractResourceConfig {
    private static final Logger logger = LoggerFactory.getLogger(PrefixesResourceConfig.class.getName());

    private static final CategoryResourceConfig instance = new CategoryResourceConfig();

    private Map<String, List<String>> nativeResourceCategories;
    private Map<String, List<String>> classNamePatternsCategories;


    @SuppressWarnings("unchecked")
    public static CategoryResourceConfig createInstance(String fileName){
        synchronized (instance){
            try{
                YamlReader reader = new YamlReader(new FileReader(fileName));
                Map categories = (Map)reader.read();
                instance.nativeResourceCategories = (Map<String, List<String>>)categories.get("resource_categories");
                instance.classNamePatternsCategories = (Map<String, List<String>>)categories.get("class_name_patterns");
                instance.inputMethodPrefixes = (List<String>) categories.get("input_method_prefixes");
                instance.targetMethodPrefixes = (List<String>) categories.get("target_method_prefixes");
                instance.mixedMethodPrefixes = (List<String>) categories.get("mixed_method_prefixes");
                instance.inputSecondaryMethodPrefixes = (List<String>) categories.get("input_secondary_method_prefixes");
                instance.mixedSecondaryMethodPrefixes = (List<String>) categories.get("mixed_secondary_method_prefixes");
                instance.targetSecondaryMethodPrefixes = (List<String>) categories.get("target_secondary_method_prefixes");
                instance.inputAllMethodPrefixes = new ArrayList<>();
                instance.inputAllMethodPrefixes.addAll(instance.inputMethodPrefixes);
                instance.inputAllMethodPrefixes.addAll(instance.inputSecondaryMethodPrefixes);
                instance.mixedAllMethodPrefixes = new ArrayList<>();
                instance.mixedAllMethodPrefixes.addAll(instance.mixedMethodPrefixes);
                instance.mixedAllMethodPrefixes.addAll(instance.mixedSecondaryMethodPrefixes);
                instance.targetAllMethodPrefixes = new ArrayList<>();
                instance.targetAllMethodPrefixes.addAll(instance.targetMethodPrefixes);
                instance.targetAllMethodPrefixes.addAll(instance.targetSecondaryMethodPrefixes);
            }catch (IOException e){
                logger.error("Could not load the yaml NR categories file: {}", fileName);
                logger.error("Details: {}", e.getCause().getMessage());
                e.printStackTrace();
                System.exit(-1);
            }
        }
        return instance;
    }

    public static CategoryResourceConfig getInstance() {
        return instance;
    }

    public Map<String, List<String>> getNativeResourceCategories() {
        return nativeResourceCategories;
    }

    public void setNativeResourceCategories(Map<String, List<String>> nativeResourceCategories) {
        this.nativeResourceCategories = nativeResourceCategories;
    }

    public Map<String, List<String>> getClassNamePatternsCategories() {
        return classNamePatternsCategories;
    }

    public void setClassNamePatternsCategories(Map<String, List<String>> classNamePatternsCategories) {
        this.classNamePatternsCategories = classNamePatternsCategories;
    }


}
