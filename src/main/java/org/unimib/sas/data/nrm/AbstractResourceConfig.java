package org.unimib.sas.data.nrm;

import java.util.List;

/**
 * Models an abstract configuration for resources.
 */
public abstract class AbstractResourceConfig implements IResourceConfig {
    protected List<String> inputMethodPrefixes;
    protected List<String> targetMethodPrefixes;
    protected List<String> mixedMethodPrefixes;

    protected List<String> inputSecondaryMethodPrefixes;
    protected List<String> mixedSecondaryMethodPrefixes;
    protected List<String> targetSecondaryMethodPrefixes;

    protected List<String> inputAllMethodPrefixes;
    protected List<String> mixedAllMethodPrefixes;
    protected List<String> targetAllMethodPrefixes;

    @Override
    public List<String> getInputMethodPrefixes() {
        return inputMethodPrefixes;
    }

    @Override
    public void setInputMethodPrefixes(List<String> inputMethodPrefixes) {
        this.inputMethodPrefixes = inputMethodPrefixes;
    }

    @Override
    public List<String> getTargetMethodPrefixes() {
        return targetMethodPrefixes;
    }

    @Override
    public void setTargetMethodPrefixes(List<String> targetMethodPrefixes) {
        this.targetMethodPrefixes = targetMethodPrefixes;
    }

    @Override
    public List<String> getMixedMethodPrefixes() {
        return mixedMethodPrefixes;
    }

    @Override
    public void setMixedMethodPrefixes(List<String> mixedMethodPrefixes) {
        this.mixedMethodPrefixes = mixedMethodPrefixes;
    }

    @Override
    public List<String> getInputSecondaryMethodPrefixes() {
        return inputSecondaryMethodPrefixes;
    }

    public void setInputSecondaryMethodPrefixes(List<String> inputSecondaryMethodPrefixes) {
        this.inputSecondaryMethodPrefixes = inputSecondaryMethodPrefixes;
    }

    @Override
    public List<String> getMixedSecondaryMethodPrefixes() {
        return mixedSecondaryMethodPrefixes;
    }

    public void setMixedSecondaryMethodPrefixes(List<String> mixedSecondaryMethodPrefixes) {
        this.mixedSecondaryMethodPrefixes = mixedSecondaryMethodPrefixes;
    }

    @Override
    public List<String> getTargetSecondaryMethodPrefixes() {
        return targetSecondaryMethodPrefixes;
    }

    public void setTargetSecondaryMethodPrefixes(List<String> targetSecondaryMethodPrefixes) {
        this.targetSecondaryMethodPrefixes = targetSecondaryMethodPrefixes;
    }

    public List<String> getAllInputMethodPrefixes() {
        return inputAllMethodPrefixes;
    }

    public List<String> getAllMixedMethodPrefixes() {
        return mixedAllMethodPrefixes;
    }

    public List<String> getAllTargetMethodPrefixes() {
        return targetAllMethodPrefixes;
    }
}
