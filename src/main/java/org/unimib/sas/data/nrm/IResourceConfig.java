package org.unimib.sas.data.nrm;

import java.util.List;
import java.util.Map;

/**
 * Models the interface of a common Resource Configuration.
 * Prefixes are modeled as a common construct across all the configurations.
 */
public interface IResourceConfig {
    List<String> getInputMethodPrefixes();
    List<String> getTargetMethodPrefixes();
    List<String> getMixedMethodPrefixes();

    void setInputMethodPrefixes(List<String> prefixes);
    void setTargetMethodPrefixes(List<String> prefixes);
    void setMixedMethodPrefixes(List<String> prefixes);

    List<String> getInputSecondaryMethodPrefixes();
    List<String> getTargetSecondaryMethodPrefixes();
    List<String> getMixedSecondaryMethodPrefixes();

    void setInputSecondaryMethodPrefixes(List<String> prefixes);
    void setTargetSecondaryMethodPrefixes(List<String> prefixes);
    void setMixedSecondaryMethodPrefixes(List<String> prefixes);

    List<String> getAllInputMethodPrefixes();
    List<String> getAllMixedMethodPrefixes();
    List<String> getAllTargetMethodPrefixes();

    Map<String, List<String>> getClassNamePatternsCategories();
    Map<String, List<String>> getNativeResourceCategories();

    void setNativeResourceCategories(Map<String, List<String>> categories);
    void setClassNamePatternsCategories(Map<String, List<String>> categories);


}
