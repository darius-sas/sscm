package org.unimib.sas.data;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Iterator;
import java.util.function.Predicate;


/**
 * Implements a filter iterator
 * @param <T> The type of the collection to iterate through.
 * @param <E> The type of the elements of the collection and of the elements of the predicates.
 */
public class Filter<T extends Iterable<E>, E> implements Iterator<E>, Iterable<E> {

    private Predicate<E> predicate;
    private Iterator<E> iterator;
    private E currentElement;

    /**
     * Creates a filter that will iterate over the elements of the given collection and will return only the
     * elements that satisfies tha predicate.
     * @param collection The collection to iterate over.
     * @param predicate The predicate the element has to satisfy in order to be returned.
     */
    public Filter(T collection, Predicate<E> predicate){
        this.predicate = predicate;
        this.iterator = collection.iterator();
    }

    /**
     * Returns {@code true} if the iteration has more elements.
     * (In other words, returns {@code true} if {@link #next} would
     * return an element rather than throwing an exception.)
     *
     * @return {@code true} if the iteration has more elements
     */
    @Override
    public boolean hasNext() {
        while(iterator.hasNext()){
            currentElement = iterator.next();
            if (predicate.test(currentElement))
                return true;
        }
        return false;
    }

    /**
     * Returns the next element in the iteration.
     *
     * @return the next element in the iteration
     * @throws NoSuchElementException if the iteration has no more elements
     */
    @Override
    public E next() {
        return currentElement;
    }

    /**
     * Returns an iterator over elements of type {@code T}.
     *
     * @return an Iterator.
     */
    @NotNull
    @Override
    public Iterator<E> iterator() {
        return this;
    }
}
