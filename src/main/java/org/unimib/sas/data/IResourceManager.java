package org.unimib.sas.data;

import org.unimib.sas.data.nrm.NativeResourceMethod;
import soot.SootClass;
import soot.SootMethod;

import java.util.List;
import java.util.Map;

/**
 * Models a general instance of a Native Resource Manager.
 * The Native Resource Manager manages a Native Resource Configuration,
 * by providing utility accessors and initializing the internal data structures.
 */
public interface IResourceManager {
    /**
     * Initializes the current instance by analyzing the current Scene
     * in search of Resources.
     */
    void initNativeResources();

    /**
     * Get the native resource methods.
     * @return the map of native resource methods where keys represent the soot method objects while
     * values are the @{@link NativeResourceMethod} of the corresponding soot method.
     */
    Map<SootMethod, NativeResourceMethod> getNativeResourceMethods();

    /**
     * Checks whether the given method is a NativeResourceMethod.
     * @param sm The soot method to check.
     * @return True if sm is a NativeResourceMethod, false otherwise.
     */
    boolean isNativeResourceMethod(SootMethod sm);

    /**
     * Gets the resource category of the given soot method.
     * @param sm The method to retrieve category for.
     * @return The string representing the category of the method.
     */
    String getNativeResourceCategory(SootMethod sm);

    /**
     * Checks wheter sc is a native resource or implements one.
     * @param sc The soot class to check for.
     * @return If sc is a Native Resource or implements/inherits one, then the Native Resource interface or class implemented by sc.
     * If sc is not a Native Resource, @{@code null} is returned.
     */
    SootClass isNativeResourceClass(SootClass sc);

    /**
     * Returns the native resource category of the given class.
     * @param sc the class to retrieve its category.
     * @return The native resource category of the given class
     */
    String getNativeResourceCategory(SootClass sc);

    /**
     * Returns the list of categories used by the current implementation.
     * @return A list containing the categories used by the current implementation.
     */
    List<String> getCategories();

    /**
     * Represents the category of some Native Resource Managers.
     */
    enum Category{
        Target,
        Input,
        Mixed,
        None
    }
}
