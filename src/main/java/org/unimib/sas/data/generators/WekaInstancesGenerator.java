package org.unimib.sas.data.generators;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unimib.sas.data.IDataset;
import org.unimib.sas.data.IDatasetGenerator;
import org.unimib.sas.data.IFeature;
import org.unimib.sas.data.IFeatureSet;
import org.unimib.sas.utils.soot.SceneWrapper;
import soot.SootClass;
import soot.SootMethod;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

/**
 * Generates a dataset using Weka Instance data frame.
 */
public class WekaInstancesGenerator implements IDatasetGenerator<Instances> {

    private final static Logger logger = LoggerFactory.getLogger(WekaInstancesGenerator.class);

    private String instancesName = "weka-instances";
    private IFeatureSet featureSet;
    private Predicate<SootClass> classPredicate;
    private Predicate<SootMethod> methodPredicate;

    private Instances instances;

    /**
     * Creates a dataset generator that filters out all the classes
     * and methods that evaluate FALSE the given predicates.
     * @param featureSet the feature set to use.
     * @param classPredicate the predicate to use for classes. If it evaluates to TRUE the class will be analyzed.
     * @param methodPredicate the predicate to use for methods. If it evaluates to TRUE the method will be analyzed.
     */
    public WekaInstancesGenerator(IFeatureSet featureSet, Predicate<SootClass> classPredicate, Predicate<SootMethod> methodPredicate){
        this.featureSet = featureSet;
        this.classPredicate = classPredicate;
        this.methodPredicate = methodPredicate;
        this.instances = null;
    }

    /**
     * Creates an instance without any filtering.
     * @param featureSet the feature set to use.
     */
    public WekaInstancesGenerator(IFeatureSet featureSet){
        this(featureSet, x-> true, x -> true);
    }

    /**
     * Calculates the features to all the classes and methods that satisfy the given predicates.
     * @param sootClasses The classes to calculate the features on.
     */
    @Override
    public void generate(Collection<SootClass> sootClasses) {
        ArrayList<Attribute> attributes = createAttributes(false);
        List<IFeature> features = this.featureSet.getFeatureList();

        this.instances = new Instances(instancesName, attributes, 1000);

        SootClass[] sootClassesArray = sootClasses.toArray(new SootClass[0]);

        for (int u=0; u < sootClassesArray.length; u++){
            if (classPredicate.test(sootClassesArray[u])){
                SootMethod[] methods = sootClassesArray[u].getMethods().toArray(new SootMethod[0]);
                for (int v = 0; v < methods.length; v++){
                    if (methodPredicate.test(methods[v])){
                        SootMethod sootMethod = methods[v];
                        DenseInstance instance = new DenseInstance(attributes.size());
                        instance.setDataset(this.instances);
                        instance.setValue(0, sootMethod.getSignature());

                        for (int i = 1; i < attributes.size(); i++){
                            instance.setValue(attributes.get(i), features.get(i-1).apply(sootMethod).getValue());
                        }
                        this.instances.add(instance);
                    }
                }
            }
        }

        // This is better than above, but for some reason triggers ConcurrentModificationException
/*        sootClasses.stream().filter(classPredicate).forEach(sc ->{
            sc.getMethods().stream().filter(methodPredicate).forEach(sootMethod -> {
                DenseInstance instance = new DenseInstance(attributes.size());
                instance.setDataset(this.instances);
                instance.setValue(0, sootMethod.getSignature());

                for (int i = 1; i < attributes.size(); i++){
                    instance.setValue(attributes.get(i), features.get(i-1).apply(sootMethod).getValue());
                }
                this.instances.add(instance);
            });
        });*/
    }

    /**
     * Generates the features on the given methods.
     * Methods not found in the Scene will be skipped.
     * @param methods the methods to calculate the features on.
     */
    public void generate(Instances methods){
        ArrayList<Attribute> attributes = createAttributes(true);
        List<IFeature> features = this.featureSet.getFeatureList();

        Attribute nameAttr = methods.attribute(IDataset.METHOD_NAME);
        Attribute tagAttr = methods.attribute(IDataset.TAG);

        this.instances = new Instances(instancesName, attributes, 1000);
        int count = 0;
        for (Instance oldInstance : methods){
            try {
                SootMethod sootMethod = SceneWrapper.v.grabMethod(oldInstance.stringValue(nameAttr));
                if (sootMethod == null || sootMethod.isPhantom())
                    throw new IllegalStateException();
                DenseInstance instance = new DenseInstance(attributes.size());

                instance.setDataset(instances);// instance has to be aware of instances otherwise weka throws exception
                instance.setValue(0, oldInstance.stringValue(nameAttr));
                instance.setValue(1, oldInstance.stringValue(tagAttr));

                for (int i = 2; i < attributes.size(); i++) {
                    instance.setValue(attributes.get(i), features.get(i-2).apply(sootMethod).getValue());
                }
                this.instances.add(instance);
            }catch (IllegalStateException ex){
                logger.warn("Could not grab method {} from Scene.", oldInstance.stringValue(nameAttr));
                count++;
            }
        }
        logger.warn("A total of {}/{} methods were not loaded because were not found in the scene.", count, methods.size());
    }

    /**
     * Returns the instances calculated by the invocation of generate.
     * @return An Instances object containing the data calculated.
     */
    @Override
    public Instances getDataset() {
        return instances;
    }

    private ArrayList<Attribute> createAttributes(boolean createClassAttr){
        ArrayList<Attribute> attributes = new ArrayList<>(this.featureSet.getFeatureList().size());
        List<IFeature> features = this.featureSet.getFeatureList();
        attributes.add(new Attribute(IDataset.METHOD_NAME, true));
        if (createClassAttr)
            attributes.add(new Attribute(IDataset.TAG, true));
        for (IFeature f : features){
            attributes.add(new Attribute(f.getFeatureName()));
        }
        return attributes;
    }
}
