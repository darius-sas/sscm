package org.unimib.sas.utils;

import heros.IFDSTabulationProblem;
import heros.InterproceduralCFG;
import org.apache.bcel.Repository;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.util.ClassPath;
import org.apache.bcel.util.SyntheticRepository;
import org.unimib.sas.data.CSVPersistency;
import org.unimib.sas.data.Dataset;
import org.unimib.sas.data.Filter;
import org.unimib.sas.data.IPersistency;
import org.unimib.sas.data.cast.CastMethodsCSVParser;
import org.unimib.sas.data.classification.ClassificationResult;
import org.unimib.sas.data.featureset.FeatureSet;
import org.unimib.sas.tdfa.TaintAnalysis;
//import org.unimib.sas.transformers.DatasetGeneratorTransformer;
import org.unimib.sas.utils.soot.SootWrapper;
import soot.*;
import soot.jimple.*;
import soot.jimple.infoflow.IInfoflow;
import soot.jimple.infoflow.Infoflow;
import soot.jimple.infoflow.InfoflowConfiguration;
import soot.jimple.infoflow.data.pathBuilders.DefaultPathBuilderFactory;
import soot.jimple.infoflow.entryPointCreators.IEntryPointCreator;
import soot.jimple.infoflow.entryPointCreators.SequentialEntryPointCreator;
import soot.jimple.infoflow.results.InfoflowResults;
import soot.jimple.infoflow.results.ResultSinkInfo;
import soot.jimple.infoflow.results.ResultSourceInfo;
import soot.jimple.infoflow.solver.cfg.IInfoflowCFG;
import soot.jimple.infoflow.sourcesSinks.manager.DefaultSourceSinkManager;
import soot.jimple.infoflow.sourcesSinks.manager.ISourceSinkManager;
import soot.jimple.toolkits.ide.JimpleIFDSSolver;
import soot.jimple.toolkits.ide.exampleproblems.IFDSLocalInfoFlow;
import soot.jimple.toolkits.ide.icfg.JimpleBasedInterproceduralCFG;
import soot.jimple.toolkits.infoflow.InfoFlowAnalysis;
import soot.options.Options;
import soot.toolkits.graph.ExceptionalUnitGraph;
import weka.classifiers.Evaluation;
import weka.classifiers.functions.SMO;
import weka.classifiers.functions.supportVector.PolyKernel;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.io.*;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.logging.Logger;

/**
 * Class used to group various methods used to test soot or general ideas.
 */
public class HackClass {

    private final static Logger LOGGER = Logger.getLogger(HackClass.class.getName());

    public static void main(String[] args){
        soot_classpath();
    }


    public static void mainTaint(String[] args){
        SootWrapper.v.withClasspath("./out/production/guinea-pigs");
        SootClass sc = Scene.v().loadClassAndSupport("tests.GuineaPigTestClass");
        Scene.v().loadNecessaryClasses();
        SootMethod sm = sc.getMethod("void storeItem(java.lang.String[])");
        System.out.println(sm.getSignature());

        Body body = sm.retrieveActiveBody();
        System.out.println("---- Jimple Body ----");
        System.out.println(body.toString());
        System.out.println("---- End JmpBody ----");
        TaintAnalysis tfa = new TaintAnalysis();
        tfa.taintAnalysisLocals(body, null);
    }

    public static HashMap<Unit, HashSet<Value>> oldTaintAnalysis(Body body){
        // Inizializzazione
        ExceptionalUnitGraph cfg = new ExceptionalUnitGraph(body);
        HashMap<Unit, HashSet<Value>> taintOut = new HashMap<>();
        HashMap<Unit, HashSet<Value>> taintIn = new HashMap<>();

        HashSet<Value> sanitized = new HashSet<>();

        Function<Unit, HashSet<Value>> taintUse = (unit)->{
            HashSet<Value> result = new HashSet<>();
            unit.getUseBoxes().forEach(valueBox -> result.add(valueBox.getValue()));
            result.retainAll(taintIn.getOrDefault(unit, new HashSet<>()));
            return result;
        };

        // Imposta i parametri come taint
        taintIn.put(cfg.getHeads().get(0), new HashSet<>(body.getParameterLocals()));
        taintOut.put(cfg.getHeads().get(0), new HashSet<>(body.getParameterLocals()));

        PatchingChain<Unit> units = body.getUnits();
        Queue<Unit> worklist = new LinkedList<>(units);

        while(!worklist.isEmpty()){
            Unit statement = worklist.remove();
            HashSet<Value> oldVal = taintOut.getOrDefault(statement, new HashSet<>());

            // Per ogni predecessore di statement, fa l'unione di tutti i taint out e salvalo nel taintIn di statement
            cfg.getUnexceptionalPredsOf(statement).forEach(u -> {
                HashSet<Value> inSet = taintIn.getOrDefault(statement, new HashSet<>());
                inSet.addAll(taintOut.getOrDefault(u, new HashSet<>()));
                taintIn.put(statement, inSet);
            });

            if(!(statement instanceof IdentityStmt)){
                // non aggiungere alla sanitizzazione in caso di identity statement: 'i0 := @parameter0: int;'
            }
            // Rimuovi variabili sanitizzate
            taintIn.getOrDefault(statement, new HashSet<>()).removeAll(sanitized);

            // Calcola il nuovo taintOut per statement
            HashSet<Value> outSet = taintOut.getOrDefault(statement, new HashSet<>());
            outSet.addAll(taintIn.getOrDefault(statement, new HashSet<>()));
            if (!taintUse.apply(statement).isEmpty()) {
                if (statement instanceof InvokeStmt){
                    InterfaceInvokeExpr invk = (InterfaceInvokeExpr) ((InvokeStmt)statement).getInvokeExpr();
                    Value invkValue = invk.getBase(); // Variabile su cui è stato invocato il metodo

                    Unit predUn = statement;
                    while(!(predUn = units.getPredOf(predUn)).equals(units.getFirst())){
                        if (predUn instanceof AssignStmt){
                            AssignStmt predUnAssignment = (AssignStmt)predUn;
                            if (predUnAssignment.getLeftOp().equals(invkValue)){
                                Value rop = predUnAssignment.getRightOp();
                                if (rop instanceof InstanceFieldRef){
                                    InstanceFieldRef instRop = (InstanceFieldRef)rop;
                                    outSet.add(instRop.getBase());
                                    outSet.add(instRop);
                                }
                            }
                        }
                    }

                }else{
                    statement.getDefBoxes().forEach(vBox -> outSet.add(vBox.getValue()));
                }
            }
            taintOut.put(statement, outSet);
            // Controlla se è cambiato, se non è cambiato allora non rivisitare questo nodo/unit
            if(!taintOut.get(statement).equals(oldVal)){
                List<Unit> successors = cfg.getUnexceptionalSuccsOf(statement);
                worklist.addAll(successors);
            }
        }
        return taintOut;
    }

    public static void jarAnalysis(){
        // Problema: carica le classi ma non i body (nemmeno con retrieve)
       SootWrapper.v
               .withWholeProgram(true)
               .withArgs("-app")
               .withPhase("cg", "enabled:false")
               .withArgs("-f", "n")
               .withArgs("-process-dir", "/home/fenn/Documenti/git/stage/sssd/soot-jars/")
               .withClasspath("/home/fenn/Documenti/git/stage/sssd/soot-jars/io.jar")
               .run();
       Scene.v().loadNecessaryClasses();
       Scene.v().getClasses();
    }

    public static void jarAnalysisPhase(){
        String className = "org.apache.commons.io.FileUtils";
        SootWrapper.v
                .withPhase("jb")
                .withPhase("wjtp")
                .withPhase("cg", "enabled:false")
                .withWholeProgram(true)
                .withPhantomRefs(false)
                .withArgs("-process-dir", "/home/fenn/Documenti/git/stage/sssd/soot-jars/io.jar")
                //.withArgs("-process-dir", "/home/fenn/Documenti/git/stage/sssd/soot-jars/httpclient.jar") Ha bisogno di dipendenze esterne
                .withArgs("-f", "n")
                .withPack("wjtp", new Transform("wjtp.jarAnalyzer", new SceneTransformer() {
                    @Override
                    protected void internalTransform(String s, Map<String, String> map) {
                        SootClass sc = Scene.v().getSootClass(className);
                        if (sc.isPhantom() || sc.getMethods().size() == 0)
                            System.err.println("La classe è phantom o non ha metodi");
                        else {
                            SootMethod sm = sc.getMethod("java.io.File getFile(java.io.File,java.lang.String[])");
                            Body body = sm.retrieveActiveBody();
                            System.out.println(sm.getSignature() + ":");
                            System.out.println(body);
                        }
                    }
                })).run();
    }

    public static void getDependenciesBCEL(){
        String myExtracp = "C:\\Users\\dariu\\Documents\\git\\sourcesinksanitationdetection\\soot-jars\\io.jar";
        ClassPath classPath = new ClassPath(ClassPath.getClassPath().concat(";").concat(myExtracp));
        Repository.setRepository(SyntheticRepository.getInstance(classPath));
        try {
            JavaClass jClazz = Repository.lookupClass("org.apache.commons.io.FileUtils");
            System.out.println("Class:" + jClazz.getClassName());
        }catch (ClassNotFoundException e){
            System.err.println("Error: " + e.getMessage());
        }
    }

    public static void infoFlow(){
        Transform tf = new Transform("wjtp.myInfoFlow", new SceneTransformer() {
            @Override
            protected void internalTransform(String s, Map<String, String> map) {
                SootClass sc = Scene.v().getSootClass("org.apache.commons.io.input.ProxyReader");
                SootMethod sm = sc.getMethods().get(2);
                InfoFlowAnalysis ifa = new InfoFlowAnalysis(false, false, true);
                ifa.getMethodInfoFlowAnalysis(sm);
            }
        });
        SootWrapper.v
                .withPhase("jb")
                .withPhase("wjtp")
                .withPhase("cg", "enabled:false")
                .withPhase("cg.spark", "enabled:true")
                .withWholeProgram(true)
                .withPhantomRefs(true)
                .withArgs("-process-dir", "./artifacts/jars/io.jar")
                .withClasspath("./soot-jars/io.jar")
                .withArgs("-f", "n")
                .withPack("wjtp", tf)
                .run();
    }

    public static void interInfoFlow(){
        Transform tf = new Transform("wjtp.myInfoFlow", new SceneTransformer() {
            @Override
            protected void internalTransform(String s, Map<String, String> map) {
                SootClass sc = Scene.v().getSootClass("org.apache.commons.io.output.FileWriterWithEncoding");
                SootMethod sm = sc.getMethods().get(16);
                SootMethod dummyMain = createDummySequantialMain(sm.getSignature());
                Scene.v().setMainClass(dummyMain.getDeclaringClass());
                Scene.v().getOrMakeFastHierarchy();
                IFDSTabulationProblem<Unit, ?, SootMethod, InterproceduralCFG<Unit,SootMethod>>
                //        lif = new MyInfoFlowAnalysis(new OnTheFlyJimpleBasedICFG(sm), sm);
                      lif = new IFDSLocalInfoFlow(new JimpleBasedInterproceduralCFG());
                JimpleIFDSSolver<?, InterproceduralCFG<Unit, SootMethod>> solver = new JimpleIFDSSolver(lif);
                solver.solve();
                solver.dumpResults();
            }
        });
        SootWrapper.v
                .withPhase("jb")
                .withPhase("wjtp")
                .withPhase("cg", "enabled:true")
                .withPhase("cg.spark", "enabled:true")
                .withWholeProgram(true)
                .withPhantomRefs(true)
                .withArgs("-process-dir", "./artifacts/jars/io.jar")
                .withClasspath("./artifacts/jars/io.jar")
                .withArgs("-f", "n")
                .withPack("wjtp", tf)
                .run();
    }

    public static SootMethod createDummySequantialMain(String methodToCall){
        List<String> methodsToCall = new ArrayList<>();
        methodsToCall.add(methodToCall);
        SequentialEntryPointCreator sepc = new SequentialEntryPointCreator(methodsToCall);
        SootMethod dummyMain = sepc.createDummyMain();
        dummyMain.setName("main");
        return dummyMain;
    }

    public static void soot_infoflow(){
        String appPath = "out/production/guinea-pigs/";        // The application path
        String libPath = "./artifacts/jars/io.jar:"+System.getProperty("java.home")+"/lib/rt.jar";                // The directory containing the unpacked library classes
        List<String> sources = new ArrayList<>();
        List<String> sinks = new ArrayList<>();
        List<String> entryPoints = new ArrayList<>();
        entryPoints.add("<infoflowtest.MyTestClass: void entryPoint()>");
        sources.add("<infoflowtest.MyTestClass: byte[] source()>");
        sources.add("<infoflowtest.MyTestClass: void source(byte[])>");
        sinks.add("<infoflowtest.MyTestClass: void execute()>");
        //sources.add("<org.apache.commons.io.input.ReaderInputStream: int read(byte[],int,int)>");
        //sinks.add("<java.io.PrintStream: void println(int)>");
        //sinks.add("<java.io.PrintStream: void print(int)>");
        //sinks.add("<infoflowtest.MyTestClass: int sink(int)>");
        //sinks.add("<infoflowtest.MyTestClass: void sink2(byte[])>");
        SequentialEntryPointCreator sepc = new SequentialEntryPointCreator(entryPoints);
        IInfoflow ifanalyzer = new Infoflow();
        ifanalyzer.getConfig().setInspectSinks(true);
        ISourceSinkManager ssm = new DefaultSourceSinkManager(sources, sinks, sources, sinks); // gli ultimi due param devono essere usati
        ifanalyzer.computeInfoflow(appPath, libPath, sepc, ssm);
    }

    public static void complete_test_analysis(){
        IPersistency csv = new CSVPersistency("./artifacts/webgoat-dataset.csv");
        Dataset dataset = (Dataset)csv.load(); // aggiornare classificazione metodi java usando NRM
        Infoflow infoflow = new Infoflow();
        List<String> sources = new ArrayList<>();
        List<String> sinks = new ArrayList<>();
        List<String> entryPoints = new ArrayList<>();
       for (SootMethod sm : dataset){
           if (dataset.getClassification(sm) == ClassificationResult.Input){
               sources.add(sm.getSignature());
           }
       }

        Predicate<SootClass> p = x -> !x.isJavaLibraryClass() && !x.getName().startsWith("jdk") && !x.getName().startsWith("com.sun") && !x.getName().startsWith("javax.swing") && !x.getName().startsWith("sun.");
        Predicate<SootMethod> p2 = x -> x.isPublic() && !x.isNative() && !x.isConstructor();
        for (SootClass sc :new Filter<>(Scene.v().getClasses(), p)){
            for (SootMethod sm : new Filter<>(sc.getMethods(), p2)){
                sinks.add(sm.getSignature());
            }
        }

        for (SootMethod sm : dataset){
            if (dataset.getClassification(sm) == ClassificationResult.None){
                entryPoints.add(sm.getSignature());
            }
        }

        IEntryPointCreator sepc = new SequentialEntryPointCreator(entryPoints);
        ISourceSinkManager ssm = new DefaultSourceSinkManager(sources, sinks, sources, sources);

        String appPath = "./artifacts/jars/webgoat";        // The application path
        StringBuilder libPath = new StringBuilder();
        Path dir = Paths.get(appPath);
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, "*.jar")){
            for (Path path : stream){
                libPath.append(path.toFile().getCanonicalPath());
                libPath.append(File.pathSeparatorChar);
            }
        }catch (IOException e){
            System.out.println("ERRORRRRRR");
        }

        dir = Paths.get(System.getProperty("java.home")+"/lib/");
        try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, "*.jar")){
            for (Path path : stream){
                libPath.append(path.toFile().getCanonicalPath());
                libPath.append(File.pathSeparatorChar);
            }
        }catch (IOException e){
            System.out.println("ERRORRRRRR");
        }
        infoflow.computeInfoflow("", libPath.toString(), sepc, ssm);
    }

    public static void webgoat_infoflow(){
        List<String> sources = new ArrayList<>();
        List<String> sinks = new ArrayList<>();
        List<String> entryPoints = new ArrayList<>();

        String appPath = "./artifacts/jars/webgoat/";        // The application path
        String jarPath = "./artifacts/jars/webgoat/";
        StringBuilder libPath = new StringBuilder();
        try(DirectoryStream<Path> stream = Files.newDirectoryStream(Paths.get(jarPath), "*.jar")){
            for (Path path : stream){
                libPath.append(path.toFile().getCanonicalFile());
                libPath.append(File.pathSeparator);
            }
        }catch (IOException e){
            System.out.println("Error loading jar files.");
            e.printStackTrace();
            System.exit(-1);
        }
        appPath = libPath.toString();
        String[] processDirs = libPath.toString().split(File.pathSeparator);                // Process Dirs will contain all the jars that will need to be processed for callgraph
        //libPath.append(System.getProperty("java.home")+"/lib/rt.jar" + File.pathSeparator);
        String classpath = libPath.toString();                                              // The classpath contains the classes to load
        SootWrapper.v.withClasspath(classpath)
                .withProcessDir(processDirs)
                .withPhase("jb")
                .withPhase("wjtp")
                .withPhase("cg", "enabled:true,verbose:true")
                .withPhase("cg.spark", "enabled:true")
                .withArgs("-f", "n")
                .withWholeProgram(true)
                .withPhantomRefs(true)
                //.withPack("wjtp", new Transform("wjtp.myP", new DatasetGeneratorTransformer("./artifacts/webgoat-dataset.csv", FeatureSet.Prefix)))
                .run();

        sinks.addAll(entryPoints);

        Infoflow infoFlow = new Infoflow();
        infoFlow.getConfig().setAccessPathLength(10);
        infoFlow.getConfig().setInspectSinks(true);
        infoFlow.getConfig().setInspectSources(true);
        infoFlow.getConfig().setIgnoreFlowsInSystemPackages(true);
        SequentialEntryPointCreator sepc = new SequentialEntryPointCreator(entryPoints);
        ISourceSinkManager ssm = new DefaultSourceSinkManager(sources, sinks, sources, sinks);
        infoFlow.computeInfoflow(appPath, libPath.toString(), sepc, ssm);
        infoFlow.getResults();
        /*sources.add("<org.apache.catalina.connector.InputBuffer: int read(char[])>");
        sources.add("<org.apache.catalina.connector.RequestFacade: java.lang.String getParameter(java.lang.String)>");
        sources.add("<org.apache.catalina.connector.CoyoteReader: int read(char[],int,int)>");
        sources.add("<org.apache.catalina.connector.InputBuffer: org.apache.coyote.Request getRequest()>");

        sinks.add("<org.apache.catalina.connector.CoyoteOutputStream: void write(byte[])>");
        sinks.add("<org.apache.catalina.connector.ResponseFacade: void setStatus(int,java.lang.String)>");
        sinks.add("<org.apache.catalina.connector.ResponseFacade: void sendRedirect(java.lang.String)>");
        sinks.add("<org.apache.catalina.core.ApplicationHttpResponse: void sendRedirect(java.lang.String)>");
        sinks.add("<org.apache.catalina.core.ApplicationResponse: void setContentType(java.lang.String)>");
        sinks.add("<org.apache.catalina.core.ApplicationHttpResponse: void setIntHeader(java.lang.String,int)>");
        entryPoints.add("<org.apache.catalina.startup.Catalina: void main(java.lang.String[])>");*/
    }

    public static void ipa_tests(){
        String appPath = "out/production/guinea-pigs/";        // The application path
        String libPath = "./artifacts/jars/io.jar:"+System.getProperty("java.home")+"/lib/rt.jar";                // The directory containing the unpacked library classes
        List<String> sources = new ArrayList<>();
        List<String> sinks = new ArrayList<>();
        List<String> entryPoints = new ArrayList<>();
        entryPoints.add("<infoflowtest.IPAtests: void entryPoint3()>");
        sources.add("<infoflowtest.IPAtests: java.lang.String inputMethod()>");
        sinks.add("<infoflowtest.IPAtests: java.lang.String noneMethod1()>");
        sinks.add("<infoflowtest.IPAtests: void noneMethod2()>");
        sinks.add("<infoflowtest.IPAtests: java.lang.String noneMethod3()>");

        SequentialEntryPointCreator sepc = new SequentialEntryPointCreator(entryPoints);
        IInfoflow ifanalyzer = new Infoflow();
        ifanalyzer.getConfig().setInspectSinks(true);
        ifanalyzer.getConfig().setInspectSources(true);
        ISourceSinkManager ssm = new DefaultSourceSinkManager(sources, sinks, sources, sinks); // gli ultimi due param devono essere usati
        ifanalyzer.computeInfoflow(appPath, libPath, sepc, ssm);
    }

    public static void tpa_tests(){
        String appPath = "out/production/guinea-pigs/";        // The application path
        String libPath = "./artifacts/jars/io.jar:"+System.getProperty("java.home")+"/lib/rt.jar";                // The directory containing the unpacked library classes
        List<String> sources = new ArrayList<>();
        List<String> sinks = new ArrayList<>();
        List<String> entryPoints = new ArrayList<>();
        //entryPoints.add("<infoflowtest.TPAtests: void entryPoint1()>");
        entryPoints.add("<infoflowtest.TPAtests: void entryPoint2()>");
        //sources.add("<infoflowtest.TPAtests: void entryPoint1()>");

        sources.add("<infoflowtest.TPAtests: void noneMethod1()>");
        sources.add("<infoflowtest.TPAtests: void noneMethod2()>");
        sources.add("<infoflowtest.TPAtests: void noneMethod3(java.lang.Object)>");
        sources.add("<infoflowtest.TPAtests: java.lang.Object noneMethod4()>");
        sources.add("<infoflowtest.TPAtests: void noneMethod5()>");

        sinks.add("<infoflowtest.TPAtests: void targetMethod(java.lang.Object)>");
        SequentialEntryPointCreator sepc = new SequentialEntryPointCreator(entryPoints);
        Infoflow ifanalyzer = new Infoflow();
        DefaultPathBuilderFactory pathFact = new DefaultPathBuilderFactory(new InfoflowConfiguration.PathConfiguration());
        ifanalyzer.setPathBuilderFactory(pathFact);
        //ifanalyzer.getConfig().setAliasingAlgorithm(InfoflowConfiguration.AliasingAlgorithm.None);
        ifanalyzer.getConfig().setInspectSinks(true);
        ifanalyzer.getConfig().setInspectSources(true);
        ifanalyzer.getConfig().setWriteOutputFiles(true);
        ISourceSinkManager ssm = new DefaultSourceSinkManager(sources, sinks); // gli ultimi due param devono essere usati
        ifanalyzer.computeInfoflow(appPath, libPath, sepc, ssm);
        InfoflowResults res = ifanalyzer.getResults();
        IInfoflowCFG cfg = null;
        for (ResultSinkInfo sinkInfo : res.getResults().keySet()){
            System.out.println("Sink " + sinkInfo.getStmt().getInvokeExpr().getMethod().getName() + " was reached by the following sources: ");
            for(ResultSourceInfo sourceInfo : res.getResults().get(sinkInfo)){
                System.out.print("\t\t");
                for (Stmt p : sourceInfo.getPath()){
                    if (p.containsInvokeExpr())
                        System.out.print(cfg.getMethodOf(p).getName() + " by invoking " + p.getInvokeExpr().getMethod().getName()  +" -> ");
                    else
                        System.out.print(cfg.getMethodOf(p).getName() + " by stmt " + p + " -> ");
                }
                System.out.println();
            }
        }
    }

    public static void parseCastCSVMethods(){

        Transform tf = new Transform("wjtp.testNRM", new SceneTransformer() {
            @Override
            protected void internalTransform(String s, Map<String, String> map) {
                CastMethodsCSVParser parser = new CastMethodsCSVParser("./validation/cast-marco/Java-methods-classification-cast.csv");
                parser.parse();
                parser.writeToCsv("./validation/cast-marco/Java-methods-classification-cast-parsed.csv");

            }
        });


        SootWrapper.v
                .withPhase("jb")
                .withPhase("wjtp")
                .withPhase("cg", "enabled:false")
                .withWholeProgram(true)
                .withPhantomRefs(true)
                .withProcessDir(Utils.listFiles("./validation/cast-marco/jars", "jar"))
                .withArgs("-f", "n")
                .withPack("wjtp", tf)
                .run();
    }

    public static void wekaClassification(){
        try {
            ConverterUtils.DataSource source = new ConverterUtils.DataSource("C:\\Users\\dariu\\git\\sourcesinksanitationdetection\\datasets\\final-dataset\\final-dataset-generated-Category-comma.csv");
            Instances dataset = source.getDataSet();
            dataset.setClass(dataset.attribute("tag"));

            System.out.println("Loaded " + dataset.size() + " instances.");
            System.out.println("Number of classes: " + dataset.numClasses());
            SMO smo = new SMO();
            smo.setC(0.35);
            smo.setKernel(new PolyKernel(dataset, 366973, 3,true));
            Evaluation eval = new Evaluation(dataset);
            eval.crossValidateModel(smo, dataset, 10, new Random(1));
            System.out.println(eval.getRevision());
            System.out.println("Precision: " +eval.weightedPrecision());
            System.out.println("Recall: " + eval.weightedRecall());
            System.out.println(eval.toSummaryString());
            System.out.println(eval.toClassDetailsString());
            System.out.println(eval.toMatrixString());
        }catch (Exception e){
            System.out.println(e.getMessage());
        }
    }

    public static void soot_classpath(){
        //TODO: write two tests, one for 1 jar case and the other for multiple jar case
        boolean single = true;
        String cp;
        String inputJar = "./scratchdir/classpath/struts/log4j-api-2.8.2.jar";
        if (single){
            cp = "./scratchdir/classpath/commons-io/io.jar";
        }else {
            cp = "./scratchdir/classpath/";
        }
        cp = String.join(File.pathSeparator, Utils.listFiles(cp, "jar"));
        cp = String.join(File.pathSeparator, cp, Paths.get(System.getProperty("java.home"), "lib", "rt.jar").toString());
        cp = String.join(File.pathSeparator, cp, Paths.get(System.getProperty("java.home"), "lib", "jce.jar").toString());
        Options.v().set_soot_classpath(cp);
        Options.v().set_process_dir(Utils.listFiles(inputJar, "jar"));

        System.out.printf("Classpath: %s\n", cp);
        System.out.printf("Input dir: %s\n", inputJar);

        //TODO: fix classpath, maybe try using soot 4 and compiling by myself

        Transformer t = new SceneTransformer() {
            @Override
            protected void internalTransform(String phaseName, Map<String, String> options) {
                String clazzName = "org.apache.commons.io.input.ProxyInputStream";
                // With forceResolve finds the correct class; TODO: check if it seen in libraryClasses
                // soot question: force resolve for all classes in classpath?
                SootClass sc = Scene.v().forceResolve(clazzName, SootClass.BODIES);
                //SootClass sc = Scene.v().getSootClassUnsafe(clazzName);
                if (sc.isPhantom())
                    System.out.printf("Class is phantom: %s\n", clazzName);
                else
                    System.out.printf("Class is NOT phantom: %s\n  and has %d methods\n", clazzName, sc.getMethods().size());

                System.out.println("Library classes: " + Scene.v().getLibraryClasses().size());
                System.out.println("Applic. classes: " + Scene.v().getApplicationClasses().size());
                System.out.println("All classes:     " + Scene.v().getClasses().size());
            }
        };
        SootWrapper.v.withPhase("wjtp")
                .withPhase("jb", "enabled:false")
                .withPhase("cg", "enabled:false,verbose:true")
                .withPhase("cg.spark", "enabled:false")
                .withArgs("-output-format", "none")
                .withWholeProgram(true)
                .withPhantomRefs(true)
                .withPack("wjtp",new Transform("wjtp.safs", t))
                .run();
    }
}