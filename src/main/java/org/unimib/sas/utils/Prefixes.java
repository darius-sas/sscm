package org.unimib.sas.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

/**
 * Contains the default prefixes used for the computations and some utility methods.
 */
public final class Prefixes {

    /**
     * The prefixes of the source methods.
     */
    public final static String[] DefaultSourceMethodPrefixes = {
            "get", "find", "retrieve", "as", "read", "pull",
            "remove", "peek", "poll", "element", "preleva", "prendi",
    };

    /**
     * The prefixes of the sink methods.
     */
    public final static String[] DefaultSinkMethodPrefixes = {
            "set", "write", "add", "put", "insert", "push",
            "offer", "save", "run", "execute", "send", "log"
    };

    /**
     * Some of the prefixes of the native resources.
     */
    public final static String[] DefaultNativeResourcePrefixes = {
            "java.io", "java.net", "javax.nio", "java.lang.System.in",
            "java.lang.System.out", "java.sql", "javax.sql"
    };

    private static List<String> nativeResourcePrefixes = null;
    private static List<String> sourceMethodPrefixes = null;
    private static List<String> sinkMethodPrefixes = null;

    /**
     * Returns a list containing all the prefixes for the resource methods.
     * @return a list of prefixes containing all the elements
     * from defaultSingMethodPrefix and DefaultSourceMethodPrefixes
     */
    public static List<String> getResourceMethodsPrefixes(){
        List<String> list = new ArrayList<>();
        list.addAll(getSinkMethodPrefixes());
        list.addAll(getSourceMethodPrefixes());
        return list;
    }


    /**
     * Checks if any of the prefixes is a prefix of the given string.
     * @param str the string to check.
     * @param prefixes the prefixes.
     * @return True if prefixes contains a prefix of str, false otherwise.
     */
    public static boolean startsWithAny(String str, Collection<String> prefixes){
        for (String prefix : prefixes){
            if (str.startsWith(prefix))
                return true;
        }
        return false;
    }

    /**
     * Checks if str starts with any of the given prefixes and returns it.
     * @param str the string to check.
     * @param prefixes the prefixes.
     * @return The prefix of str from prefixes, or null if str does not start with any of the given prefixes.
     */
    public static String startsWith(String str, Collection<String> prefixes){
        for (String prefix : prefixes){
            if (str.startsWith(prefix))
                return prefix;
        }
        return null;
    }

    /**
     * Returns the source method prefixes, if not available returns the DefaultSourceMethodPrefixes as a List.
     * @return the list of source prefixes.
     */
    public static List<String> getSourceMethodPrefixes(){
        if (sourceMethodPrefixes == null)
            return Arrays.asList(DefaultSourceMethodPrefixes);
        else
            return sourceMethodPrefixes;
    }

    /**
     * Returns the sink method prefixes, if not available returns the DefaultSinkMethodPrefixes as a List.
     * @return the list of sink prefixes.
     */
    public static List<String> getSinkMethodPrefixes(){
        if (sinkMethodPrefixes == null)
            return Arrays.asList(DefaultSinkMethodPrefixes);
        else
            return sinkMethodPrefixes;
    }

    /**
     * Returns the native resource prefixes, if not available returns the DefaultNativeResourcePrefixes as a List.
     * @return the list of prefixes of native resources.
     */
    public static List<String> getNativeResourcePrefixes(){
        if (nativeResourcePrefixes == null)
            return Arrays.asList(DefaultNativeResourcePrefixes);
        else
            return nativeResourcePrefixes;
    }

    public static void setNativeResourcePrefixes(List<String> nativeResourcePrefixes) {
        Prefixes.nativeResourcePrefixes = nativeResourcePrefixes;
    }

    public static void setSinkMethodPrefixes(List<String> sinkMethodPrefixes) {
        Prefixes.sinkMethodPrefixes = sinkMethodPrefixes;
    }

    public static void setSourceMethodPrefixes(List<String> sourceMethodPrefixes) {
        Prefixes.sourceMethodPrefixes = sourceMethodPrefixes;
    }
}
