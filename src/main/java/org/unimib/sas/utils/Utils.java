package org.unimib.sas.utils;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Utils {
    /**
     * Recursively lists the files in a directory using an extension.
     * If the directory param is a file with the given extension, only the file is returned.
     * @param directory The directory to list.
     * @param extension The extension files must respect in order to be returned.
     * @return A list containing the canonical paths of the file in the given directory.
     */
    public static List<String> listFiles(String directory, String extension){
        List<String> fileList = new ArrayList<>();
        try{
            File targetDir = new File(directory);
            if (!targetDir.exists())
                throw new RuntimeException("File or directory does not exists.");

            if (targetDir.isDirectory())
                FileUtils.listFiles(targetDir, new String[]{extension}, true).forEach(x -> {
                    try {
                        fileList.add(x.getCanonicalPath());
                    }catch(IOException e){}
            });
            else
                fileList.add(directory);
        }catch (RuntimeException e ){
            System.out.println("Error loading " + extension + " files from: " + directory);
            e.printStackTrace();
            System.exit(-1);
        }
        return fileList;
    }
}
