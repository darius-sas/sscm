package org.unimib.sas.commands;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.unimib.sas.utils.Utils;
import org.unimib.sas.utils.soot.SootWrapper;
import soot.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * Implements the command for listing all the methods from a certain JAR file.
 */
public class ListCommand extends AbstractCommand {
    private final Option TARGETJAR;
    private final Option OUTFILE;

    private List<String> TARGET_JAR;
    private String OUT_FILE;

    public ListCommand() {
        super("list");

        TARGETJAR = Option.builder("f")
                .desc("The JAR file to list the methods of.")
                .argName("jar-or-folder")
                .hasArg()
                .required()
                .build();

        OUTFILE = Option.builder("o")
                .desc("The file where to write the method list.")
                .argName("out-file")
                .hasArg()
                .required()
                .build();

        OPTIONS.addOption(TARGETJAR);
        OPTIONS.addOption(OUTFILE);
    }

    /**
     * Loads the commands from the CommandLine object into the current object in order
     * to ease the execution of the command.
     *
     * @param cmdLine the command line containing the commands.
     */
    @Override
    protected void loadOptions(CommandLine cmdLine) {
        if (cmdLine.hasOption(TARGETJAR.getOpt())){
            TARGET_JAR = Utils.listFiles(
                    cmdLine.getOptionValue(TARGETJAR.getOpt()),
                    "jar");
        }else {
            throw new RuntimeException(String.format("The target jar (-%s) option is missing.", TARGETJAR.getOpt()));
        }

        if (cmdLine.hasOption(OUTFILE.getOpt())){
            OUT_FILE = cmdLine.getOptionValue(OUTFILE.getOpt());
        }else {
            throw new RuntimeException(String.format("The output file (-%s) option is missing.", OUTFILE.getOpt()));
        }
    }

    /**
     * Returns the transformer object that can be fed to soot to be executed as a within its phases.
     *
     * @return
     */
    @Override
    public Transformer getTransformer() {
        return new SceneTransformer() {
            @Override
            protected void internalTransform(String phaseName, Map<String, String> options) {
                Collection<SootClass> classes = Scene.v().getApplicationClasses();
                List<SootMethod> methods = new ArrayList<>(classes.size() * 5);
                for (SootClass sc : classes){
                    methods.addAll(sc.getMethods());
                }
                try(PrintWriter pw = new PrintWriter(new FileWriter(OUT_FILE, false))){
                    for (SootMethod sm : methods)
                        pw.printf("\"%s\"%s", sm.getSignature(), System.lineSeparator());
                }catch (IOException e){
                    throw new RuntimeException(
                            String.format("An error occurred while writing on %s:\n%s\n", OUT_FILE, e.getMessage()));
                }
            }
        };
    }

    /**
     * The implementers of this methods will execute the command that it is representing.
     * It must take care of the correct configuration of soot.
     */
    @Override
    public void run() {
        SootWrapper.v.withPhase("wjtp")
                .withPhase("jb","enabled:false" )
                .withPhase("cg", "enabled:false,verbose:true")
                .withPhase("cg.spark", "enabled:false")
                .withArgs("-output-format", "none")
                .withProcessDir(TARGET_JAR)
                .withWholeProgram(true)
                .withPhantomRefs(true);
        Transform t = new Transform("wjtp.command." + getCommandName(), getTransformer());
        SootWrapper.v.withPack("wjtp", t).run();
    }
}
