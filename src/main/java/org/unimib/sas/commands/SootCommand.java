package org.unimib.sas.commands;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.unimib.sas.data.featureset.FeatureSet;
import org.unimib.sas.data.nrm.ResourceManagerType;
import org.unimib.sas.utils.Utils;
import org.unimib.sas.utils.soot.SootWrapper;
import soot.Transform;

import java.io.File;
import java.util.Arrays;

/**
 * Models commands that require soot to be executed.
 */
public abstract class SootCommand extends AbstractCommand {

    protected String CLASSPATH_;
    protected FeatureSet FEATURE_SET;
    protected ResourceManagerType NRM;
    //protected String TARGET_FOLDER;
    protected String RESOURCE_FILE;

    private final Option CLASSPATH;
    private final Option FEATURESET;
    //private final Option TARGETFOLDER;
    protected final Option RESOURCEFILE;

    public SootCommand(String commandName){

        super(commandName);

        CLASSPATH = Option.builder("cp")
                .argName("classpath-folder")
                .desc("The classpath for the analysis.")
                .hasArg()
                .required(true)
                .build();

        FEATURESET = Option.builder("fs")
                .argName("feature-set-name")
                .desc("(optional) The set of features to calculate. Accepted values are: " + Arrays.toString(FeatureSet.values()) + " default and suggested is 'Category'.")
                .hasArg()
                .build();

/*        TARGETFOLDER = Option.builder("tf")
                .argName("target-folder")
                .desc("The folder containing the JARs to analyze.")
                .hasArg()
                .build();*/

        RESOURCEFILE = Option.builder("r")
                .argName("resource-file")
                .hasArg()
                .required()
                .desc("A yaml file containing the description of the resources.")
                .build();

        OPTIONS.addOption(RESOURCEFILE);

        OPTIONS.addOption(CLASSPATH);
        OPTIONS.addOption(FEATURESET);
        //OPTIONS.addOption(TARGETFOLDER);
    }

    /**
     * This method will be invoked before running the Transformer from getTransformer() to
     * allow subclasses correctly setup soot.
     * The default implementation provides a default configuration.
     */
    protected void setUpSoot(){
        SootWrapper.v.withPhase("wjtp")
                .withPhase("jb","enabled:false" )
                .withPhase("cg", "enabled:false,verbose:true")
                .withPhase("cg.spark", "enabled:false")
                .withArgs("-output-format", "none")
                .withWholeProgram(true)
                .withPhantomRefs(true);
    }

    /**
     * Allows the subclasses to add extra configuration to the default one.
     */
    protected abstract void doOtherSootSetup();

    /**
     * Loads the common soot commands before invoking doLoadOtherOptions method, where
     * subclasses can load their own commands.
     * @param cmdLine the command line containing the commands.
     */
    @Override
    protected final void loadOptions(CommandLine cmdLine) {
        if (cmdLine.hasOption(CLASSPATH.getOpt())){
            CLASSPATH_ = cmdLine.getOptionValue(CLASSPATH.getOpt());
            if (new File(CLASSPATH_).isDirectory()) {
                SootWrapper.v.withClasspath(Utils.listFiles(CLASSPATH_, "jar").toArray(new String[]{}));
            }else {
                SootWrapper.v.withClasspath(CLASSPATH_);
            }
        }

        if (cmdLine.hasOption(RESOURCEFILE.getOpt())){
            RESOURCE_FILE = cmdLine.getOptionValue(RESOURCEFILE.getOpt());
            File rsfile = new File(RESOURCE_FILE);
            if (!rsfile.exists() || rsfile.isDirectory()){
                throw new RuntimeException("Resource file argument (-r) does not exists.");
            } else if (!RESOURCE_FILE.endsWith(".yaml") && !RESOURCE_FILE.endsWith(".yml")){
                throw new RuntimeException("Resource file argument (-r) must be a yaml file.");
            }
        }


        if (cmdLine.hasOption(FEATURESET.getOpt())){
            try {
                FEATURE_SET = FeatureSet.fromString(cmdLine.getOptionValue(FEATURESET.getOpt()));
            }catch (IllegalArgumentException e){
                throw new RuntimeException("Malformed feature set value. Accepted are: " + Arrays.toString(FeatureSet.values()));
            }
        }else {
            FEATURE_SET = FeatureSet.Category;
        }
        if (FEATURE_SET.equals(FeatureSet.Category)){
            NRM = ResourceManagerType.Category;
        }else{
            NRM = ResourceManagerType.Normal;
        }

        doLoadOtherOptions(cmdLine);
    }

    /**
     * Subclasses can implement this method to load their own commands from the CommandLine object.
     * @param cmdLine the command line containing the commands.
     */
    protected abstract void doLoadOtherOptions(CommandLine cmdLine);

    /**
     * Runs the getTransformer() within a default configuration of soot.
     */
    public void run(){
        Transform t = new Transform("wjtp.command."+this.getCommandName(), getTransformer());
        setUpSoot();
        doOtherSootSetup();
        logger.info("Soot setup completed. Running '{}' command.", getCommandName());
        SootWrapper.v.withPack("wjtp", t).run();
    }
}
