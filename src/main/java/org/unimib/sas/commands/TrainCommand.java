package org.unimib.sas.commands;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Option;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.unimib.sas.data.IDataset;
import org.unimib.sas.data.classification.WekaClassification;
import soot.SceneTransformer;
import soot.Transformer;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Map;

/**
 * Implements the 'train' command.
 */
public class TrainCommand extends AbstractCommand {

    private final static Logger logger = LoggerFactory.getLogger(TrainCommand.class);

    private String TRAIN_FILE;
    private String OUTPUT_MODEL;
    private String[] WEKA_OPTIONS;

    private final Option TRAINFILE;
    private final Option OUTPUTMODEL;
    private final Option WEKAOPTIONS;


    public TrainCommand(){
        super("train");

        TRAINFILE = Option.builder("t")
                .argName("training-file")
                .required()
                .hasArg()
                .desc("The training data file.")
                .build();

        OUTPUTMODEL = Option.builder("o")
                .argName("output-model-file")
                .required()
                .hasArg()
                .desc("The output model file.")
                .build();

        WEKAOPTIONS = Option.builder("m")
                .argName("weka-model-opts")
                .hasArgs()
                .desc("(Optional)The WEKA options for instantiating the classifier. Check JavaDoc for weka.classifiers.Classifier for more info." +
                        " By default a SVM with polynomial kernel is used.")
                .build();

        this.OPTIONS.addOption(TRAINFILE);
        this.OPTIONS.addOption(OUTPUTMODEL);
        this.OPTIONS.addOption(WEKAOPTIONS);
    }

    @Override
    protected void loadOptions(CommandLine args) {
        String tmp;
        if (args.hasOption(TRAINFILE.getOpt())){
            tmp = args.getOptionValue(TRAINFILE.getOpt());
            if (!Files.exists(new File(tmp).toPath())){
                throw new RuntimeException("Training data file does not exist.");
            }
            TRAIN_FILE = tmp;
        }

        if (args.hasOption(OUTPUTMODEL.getOpt())){
            tmp = args.getOptionValue(OUTPUTMODEL.getOpt());
            OUTPUT_MODEL = tmp;
        }

        if (args.hasOption(WEKAOPTIONS.getOpt())){
            WEKA_OPTIONS = args.getOptionValues(WEKAOPTIONS.getOpt());
        }else {
            WEKA_OPTIONS = new String[]{
                    "weka.classifiers.functions.SMO",
                    "-C", "0.35",
                    "-K", "weka.classifiers.functions.supportVector.PolyKernel -C 250007 -E 3 -L"};
        }
    }


    @Override
    public Transformer getTransformer(){
        Transformer transformer = new SceneTransformer() {
            @Override
            protected void internalTransform(String s, Map<String, String> map) {
                try {
                    logger.info("Reading training data from {}", TRAIN_FILE);

                    Instances trainingSet = ConverterUtils.DataSource.read(TRAIN_FILE);
                    WekaClassification classifier = new WekaClassification(WEKA_OPTIONS[0], Arrays.copyOfRange(WEKA_OPTIONS, 1, WEKA_OPTIONS.length));
                    trainingSet.setClass(trainingSet.attribute(IDataset.TAG));

                    logger.info("Using '{}' as class attribute.", trainingSet.classAttribute().name());
                    logger.info("Training started...");

                    classifier.train(trainingSet);

                    logger.info("Training completed, writing model on file {}", OUTPUT_MODEL);
                    FileOutputStream fos = new FileOutputStream(OUTPUT_MODEL);
                    WekaClassification.serialize(classifier, fos);
                    fos.close();
                }catch (Exception e){
                    logger.error("An error occurred: {}", e.getMessage());
                    System.exit(-1);
                }
            }
        };
        return transformer;
    }

    public void run(){
        ((SceneTransformer)getTransformer()).transform();
    }
}
